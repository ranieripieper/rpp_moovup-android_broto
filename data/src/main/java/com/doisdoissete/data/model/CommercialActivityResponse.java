package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by broto on 10/9/15.
 */
public class CommercialActivityResponse {

    @Expose
    @SerializedName("commercial_activities")
    private List<CommercialActivityEntity> commercialActivityEntityList;

    @Expose
    private Meta meta;

    public List<CommercialActivityEntity> getCommercialActivityEntityList() {
        return commercialActivityEntityList;
    }

    public void setCommercialActivityEntityList(List<CommercialActivityEntity> commercialActivityEntityList) {
        this.commercialActivityEntityList = commercialActivityEntityList;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
