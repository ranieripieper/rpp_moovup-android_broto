package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by broto on 10/15/15.
 */
public class GoalEntity extends RealmObject {

    @Expose
    private String title;

    @Expose
    @SerializedName("periodicity_type")
    private String periodicityType;

    @Expose
    @SerializedName("target_date")
    private String targetDate;

    @Expose
    @SerializedName("total_minutes")
    private String totalMinutes;

    @Expose
    @SerializedName("public")
    private boolean mPublic;

    @Expose
    @SerializedName("current_weight")
    private float currentWeight;

    @Expose
    @SerializedName("target_weight")
    private float targetWeight;

    @Expose
    @SerializedName("category_id")
    private int categoryId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPeriodicityType() {
        return periodicityType;
    }

    public void setPeriodicityType(String periodicityType) {
        this.periodicityType = periodicityType;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public boolean ismPublic() {
        return mPublic;
    }

    public void setmPublic(boolean mPublic) {
        this.mPublic = mPublic;
    }

    public float getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(float currentWeight) {
        this.currentWeight = currentWeight;
    }

    public float getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(float targetWeight) {
        this.targetWeight = targetWeight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

}
