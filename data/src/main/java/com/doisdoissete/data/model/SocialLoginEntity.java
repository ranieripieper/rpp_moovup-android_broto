package com.doisdoissete.data.model;

import com.doisdoissete.data.Const;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by broto on 10/1/15.
 */
public class SocialLoginEntity {

    @Expose
    @SerializedName("access_token")
    private String accessToken;

    @Expose
    private String provider = Const.PROVIDER;

    public SocialLoginEntity(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
