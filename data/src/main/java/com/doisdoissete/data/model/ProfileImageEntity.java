package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;

/**
 * Created by broto on 10/5/15.
 */
public class ProfileImageEntity extends RealmObject {

    @Expose
    private String thumb;

    @Expose
    private String medium;

    public ProfileImageEntity() {
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }
}
