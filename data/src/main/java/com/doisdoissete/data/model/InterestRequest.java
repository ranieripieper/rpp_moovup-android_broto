package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;

/**
 * Created by broto on 11/5/15.
 */
public class InterestRequest {

    @Expose
    private InterestEntity interest;

    public InterestRequest() {
    }

    public InterestRequest(InterestEntity interest) {
        this.interest = interest;
    }

    public InterestEntity getInterest() {
        return interest;
    }

    public void setInterest(InterestEntity interest) {
        this.interest = interest;
    }
}
