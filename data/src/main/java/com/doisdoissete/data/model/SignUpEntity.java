package com.doisdoissete.data.model;

import com.doisdoissete.data.Const;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by broto on 10/1/15.
 */
public class SignUpEntity<T> {

    @Expose
    @SerializedName("user")
    private T data;

    @Expose
    private String provider = Const.PROVIDER;

    public SignUpEntity(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
