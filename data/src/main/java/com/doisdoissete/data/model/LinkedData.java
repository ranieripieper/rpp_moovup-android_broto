package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by broto on 11/6/15.
 */
public class LinkedData {

    @Expose
    private List<CategoryEntity> categories;

    @Expose
    private List<UserEntityNew> users;

    public List<CategoryEntity> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryEntity> categories) {
        this.categories = categories;
    }

    public List<UserEntityNew> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntityNew> users) {
        this.users = users;
    }
}
