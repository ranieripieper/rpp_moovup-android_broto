package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by broto on 10/1/15.
 */
public class CompanyEntity {

    @Expose
    @SerializedName("company_name")
    private String companyName;

    @Expose
    @SerializedName("foundation_date")
    private String foundationDate;

    @Expose
    @SerializedName("commercial_activity_id")
    private int commercialActivityId;

    @Expose
    private String email;

    @Expose
    @SerializedName("profile_type")
    private String profileType = "company";

    @Expose
    private String password;

    @Expose
    @SerializedName("password_confirmation")
    private String passwordConfirmation;

    public CompanyEntity(String companyName, String foundationDate, int commercialActivityId, String email, String password, String passwordConfirmation) {
        this.companyName = companyName;
        this.foundationDate = foundationDate;
        this.commercialActivityId = commercialActivityId;
        this.email = email;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(String foundationDate) {
        this.foundationDate = foundationDate;
    }

    public int getCommercialActivityId() {
        return commercialActivityId;
    }

    public void setCommercialActivityId(int commercialActivityId) {
        this.commercialActivityId = commercialActivityId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
