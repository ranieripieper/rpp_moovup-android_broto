package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by broto on 9/21/15.
 */
public class CareerResponse {

    @Expose
    @SerializedName("carrers")
    private List<CareerEntity> careerEntityList;

    @Expose
    private Meta meta;

    public CareerResponse() {
    }

    public List<CareerEntity> getCarrerEntityList() {
        return careerEntityList;
    }

    public void setCarrerEntityList(List<CareerEntity> careerEntityList) {
        this.careerEntityList = careerEntityList;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

}
