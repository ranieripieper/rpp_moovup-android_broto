package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;

/**
 * Created by broto on 10/20/15.
 */
public class GoalRequest {

    @Expose
    private GoalEntity goal;

    public GoalRequest(GoalEntity goal) {
        this.goal = goal;
    }

    public GoalEntity getGoal() {
        return goal;
    }

    public void setGoal(GoalEntity goal) {
        this.goal = goal;
    }
}
