package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by broto on 11/5/15.
 */
public class FeedEntity {

    @Expose
    private List<QuestionEntity> questions;

    @Expose
    @SerializedName("linked_data")
    private LinkedData linkedData;

    @Expose
    private Meta meta;

    public List<QuestionEntity> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionEntity> questions) {
        this.questions = questions;
    }

    public LinkedData getLinkedData() {
        return linkedData;
    }

    public void setLinkedData(LinkedData linkedData) {
        this.linkedData = linkedData;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
