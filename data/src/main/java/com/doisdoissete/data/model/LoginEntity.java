package com.doisdoissete.data.model;

import com.doisdoissete.data.Const;
import com.google.gson.annotations.Expose;

/**
 * Created by broto on 9/21/15.
 */
public class LoginEntity {

    @Expose
    private String provider = Const.PROVIDER;

    @Expose
    private String email;

    @Expose
    private String password;

    public LoginEntity(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
