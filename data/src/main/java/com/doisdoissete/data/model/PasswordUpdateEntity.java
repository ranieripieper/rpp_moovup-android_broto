package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by broto on 9/21/15.
 */
public class PasswordUpdateEntity {

    @Expose
    private String password;

    @Expose
    @SerializedName("password_confirmation")
    private String passwordConfirmation;

    public PasswordUpdateEntity(String password, String passwordConfirmation) {
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
