package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by broto on 11/3/15.
 */
public class InterestEntity {

    @Expose
    @SerializedName("category_id")
    private int categoryId;

    @Expose
    @SerializedName("max_answers_count")
    private int maxAnswersCount;

    public InterestEntity() {
    }

    public InterestEntity(int categoryId, int maxAnswersCount) {
        this.categoryId = categoryId;
        this.maxAnswersCount = maxAnswersCount;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getMaxAnswersCount() {
        return maxAnswersCount;
    }

    public void setMaxAnswersCount(int maxAnswersCount) {
        this.maxAnswersCount = maxAnswersCount;
    }
}
