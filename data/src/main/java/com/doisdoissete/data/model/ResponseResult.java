package com.doisdoissete.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by broto on 9/20/15.
 */
public class ResponseResult {

    @Expose
    private boolean success;

    @Expose
    private boolean error;

    @Expose
    private boolean updated;

    @Expose
    @SerializedName("user_exists")
    private boolean userExists;

    @Expose
    @SerializedName("changed_attributes")
    private List<String> changedAttributes;

    @Expose
    @SerializedName("status_code")
    private int statusCode;

    @Expose
    @SerializedName("user_data")
    private UserData userData;

    @Expose
    @SerializedName("auth_data")
    private AuthData authData;

    @Expose
    private UserEntityNew user;

    @Expose
    @SerializedName("new_user")
    private boolean newUser;

    @Expose
    @SerializedName("reset_password_token")
    private String resetPasswordToken;

    @Expose
    private List<String> errors = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public AuthData getAuthData() {
        return authData;
    }

    public void setAuthData(AuthData authData) {
        this.authData = authData;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public List<String> getChangedAttributes() {
        return changedAttributes;
    }

    public void setChangedAttributes(List<String> changedAttributes) {
        this.changedAttributes = changedAttributes;
    }

    public UserEntityNew getUser() {
        return user;
    }

    public void setUser(UserEntityNew user) {
        this.user = user;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public boolean isUserExists() {
        return userExists;
    }

    public void setUserExists(boolean userExists) {
        this.userExists = userExists;
    }

    public class AuthData {

        @Expose
        @SerializedName("auth_token")
        private String authToken;

        @Expose
        private String provider;

        @Expose
        @SerializedName("expires_at")
        private Date expiresAt;

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public Date getExpiresAt() {
            return expiresAt;
        }

        public void setExpiresAt(Date expiresAt) {
            this.expiresAt = expiresAt;
        }
    }

    public class UserData {

        @Expose
        private int id;

        @Expose
        private String username;

        @Expose
        @SerializedName("first_name")
        private String firstName;

        @Expose
        @SerializedName("last_name")
        private String lastName;

        @Expose
        @SerializedName("full_name")
        private String fullName;

        @Expose
        @SerializedName("profile_image_url")
        private String profileImageUrl;

        @Expose
        @SerializedName("profile_type")
        private String profileType;

        @Expose
        @SerializedName("oauth_provider")
        private String oAuthProvider;

        @Expose
        @SerializedName("oauth_provider_uid")
        private String oAuthProviderUid;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getProfileImageUrl() {
            return profileImageUrl;
        }

        public void setProfileImageUrl(String profileImageUrl) {
            this.profileImageUrl = profileImageUrl;
        }

        public String getProfileType() {
            return profileType;
        }

        public void setProfileType(String profileType) {
            this.profileType = profileType;
        }

        public String getoAuthProvider() {
            return oAuthProvider;
        }

        public void setoAuthProvider(String oAuthProvider) {
            this.oAuthProvider = oAuthProvider;
        }

        public String getoAuthProviderUid() {
            return oAuthProviderUid;
        }

        public void setoAuthProviderUid(String oAuthProviderUid) {
            this.oAuthProviderUid = oAuthProviderUid;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }
    }
}
