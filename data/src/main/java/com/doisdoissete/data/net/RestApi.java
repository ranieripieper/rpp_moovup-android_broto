package com.doisdoissete.data.net;

/**
 * Created by broto on 8/28/15.
 */
public interface RestApi {

    APIService getApiService();
}
