package com.doisdoissete.data.net;

import android.util.Log;

import com.facebook.stetho.okhttp.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import javax.inject.Inject;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by broto on 8/28/15.
 */
public class PublicApi implements RestApi{

    private APIService apiService;

    @Inject
    public PublicApi() {
        OkHttpClient client = new OkHttpClient();
        client.networkInterceptors().add(new StethoInterceptor());

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        RestAdapter.Builder restBuilder = new RestAdapter.Builder();
        restBuilder.setEndpoint(APIService.API_BASE_URL);
        restBuilder.setClient(new OkClient(client));
        restBuilder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Content-Type", "application/json");
            }
        });
        restBuilder.setConverter(new GsonConverter(gson)).build();
        restBuilder.setLogLevel(RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
            public void log(String msg) {
                Log.i("retrofit", msg);
            }
        });

        RestAdapter restAdapter = restBuilder.build();
        apiService=  restAdapter.create(APIService.class);
    }

    @Override
    public APIService getApiService() {
        return apiService;
    }
}
