package com.doisdoissete.data.net;


import com.doisdoissete.data.model.CareerResponse;
import com.doisdoissete.data.model.CategoryResponse;
import com.doisdoissete.data.model.CommercialActivityResponse;
import com.doisdoissete.data.model.CompanyEntity;
import com.doisdoissete.data.model.EmailEntity;
import com.doisdoissete.data.model.FeedEntity;
import com.doisdoissete.data.model.GoalEntity;
import com.doisdoissete.data.model.GoalRequest;
import com.doisdoissete.data.model.HobbiesResponse;
import com.doisdoissete.data.model.InterestEntity;
import com.doisdoissete.data.model.InterestRequest;
import com.doisdoissete.data.model.LoginEntity;
import com.doisdoissete.data.model.PasswordUpdateEntity;
import com.doisdoissete.data.model.PersonEntity;
import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.model.SignUpEntity;
import com.doisdoissete.data.model.SocialLoginEntity;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by broto on 8/15/15.
 */
public interface APIService {

    String API_BASE_URL = "http://moovup-api-staging.herokuapp.com/api/v1";

    // GET

    @GET("/users/check_email")
    void checkEmail(@Query("email") String email, Callback<CareerResponse> callback);

    @GET("/carrers")
    void getCareers(@Query("per_page") int perPage, Callback<CareerResponse> callback);

    @GET("/hobbies")
    void getAllHobbies(Callback<HobbiesResponse> callback);

    @GET("/commercial_activities")
    void getAllCommercialActivities(Callback<CommercialActivityResponse> callback);

    @GET("/categories")
    void getCategories(Callback<CategoryResponse> callback);

    @GET("/users/me/feed/questions")
    void getNewsFeed(Callback<FeedEntity> callback);

    // POST

    @POST("/users")
    void signUpPerson(@Body SignUpEntity<PersonEntity> signUp, Callback<ResponseResult> callback);

    @POST("/users")
    void signUpCompany(@Body SignUpEntity<CompanyEntity> signUp, Callback<ResponseResult> callback);

    @POST("/users/auth/facebook")
    void loginFacebook(@Body SocialLoginEntity login, Callback<ResponseResult> callback);

    @POST("/users/auth/google_plus")
    void loginGoogle(@Body SocialLoginEntity login, Callback<ResponseResult> callback);

    @POST("/users/auth")
    void login(@Body LoginEntity loginEntity, Callback<ResponseResult> callback);

    @POST("/users/password_reset")
    void requestRecover(@Body EmailEntity emailEntitya, Callback<ResponseResult> callback);

    @POST("/users/auth/token")
    void userTokenAuthentication(@Body LoginEntity loginEntity);

    @POST("/goals")
    void addGoal(@Body GoalRequest goal, Callback<ResponseResult> callback);

    @POST("/users/me/interests")
    void addInterest(@Body InterestRequest interestRequest, Callback<ResponseResult> callback);

    // PUT

    @PUT("/users/password_reset/{token}")
    void updatePassword(@Body PasswordUpdateEntity passwordUpdateEntity, @Path("token") String token );

    @PUT("/users/me/picture")
    @Multipart
    void updatePhoto(@Part("user[profile_image]") TypedFile photoFile, @Part("filename") TypedString filename, Callback<ResponseResult> callback);

    // DELETE

    @DELETE("/users/auth")
    void logout();

    @DELETE("/users/me/interests")
    void removeInterest(@Query("category_id") int categoryId, Callback<ResponseResult> callback);
}
