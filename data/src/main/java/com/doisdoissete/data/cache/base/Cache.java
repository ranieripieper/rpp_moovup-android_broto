package com.doisdoissete.data.cache.base;

import java.util.List;

/**
 * Created by broto on 8/16/15.
 */
public interface Cache<T> {

    void put(T t);

    void putAll(List<T> t);

    void delete(int id);

    void deleteAll();

    T get(int id);

    List<T> getAll();
}
