package com.doisdoissete.data.cache;

import android.content.SharedPreferences;

import com.doisdoissete.data.cache.base.Cache;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 10/5/15.
 */
public class TokenCache implements Cache<String> {

    public static final String PROP_TOKEN = "prop_token";

    private SharedPreferences sharedPreferences;

    @Inject
    public TokenCache(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void put(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PROP_TOKEN, token);
        editor.apply();
    }

    @Override
    public void putAll(List<String> t) {

    }

    @Override
    public void delete(int id) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PROP_TOKEN, "");
        editor.apply();
    }

    @Override
    public void deleteAll() {
        delete(0);
    }

    @Override
    public String get(int id) {
        return sharedPreferences.getString(PROP_TOKEN, "");
    }

    @Override
    public List<String> getAll() {
        return null;
    }
}
