package com.doisdoissete.data.cache;

import com.doisdoissete.data.cache.base.BaseCache;
import com.doisdoissete.data.model.UserEntityNew;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by broto on 11/5/15.
 */
public class UserCacheNew extends BaseCache<UserEntityNew> {

    @Inject
    public UserCacheNew(Realm realm) {
        super(realm);
    }

    @Override
    public Class<UserEntityNew> getReferenceClass() {
        return UserEntityNew.class;
    }
}
