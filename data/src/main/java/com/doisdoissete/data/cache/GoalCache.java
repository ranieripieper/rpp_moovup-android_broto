package com.doisdoissete.data.cache;

import com.doisdoissete.data.cache.base.BaseCache;
import com.doisdoissete.data.model.GoalEntity;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by broto on 10/15/15.
 */
public class GoalCache extends BaseCache<GoalEntity> {

    @Inject
    public GoalCache(Realm realm) {
        super(realm);
    }

    @Override
    public Class<GoalEntity> getReferenceClass() {
        return GoalEntity.class;
    }
}

