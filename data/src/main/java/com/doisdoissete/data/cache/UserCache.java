package com.doisdoissete.data.cache;

import com.doisdoissete.data.cache.base.BaseCache;
import com.doisdoissete.data.model.UserEntity;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by broto on 9/22/15.
 */
public class UserCache extends BaseCache<UserEntity> {

    private TokenCache tokenCache;

    @Inject
    public UserCache(Realm realm, TokenCache tokenCache) {
        super(realm);
        this.tokenCache = tokenCache;
    }

    @Override
    public void put(UserEntity user) {
        tokenCache.put(user.getAuthToken());
    }

    @Override
    public Class<UserEntity> getReferenceClass() {
        return null;
    }
}

