package com.doisdoissete.data.cache;

import com.doisdoissete.data.cache.base.BaseCache;
import com.doisdoissete.data.model.CommercialActivityEntity;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by broto on 10/9/15.
 */
public class CommercialActivityCache extends BaseCache<CommercialActivityEntity> {

    @Inject
    public CommercialActivityCache(Realm realm) {
        super(realm);
    }

    @Override
    public Class<CommercialActivityEntity> getReferenceClass() {
        return CommercialActivityEntity.class;
    }
}
