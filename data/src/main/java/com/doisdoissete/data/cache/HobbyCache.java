package com.doisdoissete.data.cache;

import com.doisdoissete.data.cache.base.BaseCache;
import com.doisdoissete.data.model.HobbyEntity;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by broto on 10/10/15.
 */
public class HobbyCache extends BaseCache<HobbyEntity> {

    @Inject
    public HobbyCache(Realm realm) {
        super(realm);
    }

    @Override
    public Class<HobbyEntity> getReferenceClass() {
        return HobbyEntity.class;
    }
}

