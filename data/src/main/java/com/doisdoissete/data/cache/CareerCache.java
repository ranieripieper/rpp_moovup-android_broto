package com.doisdoissete.data.cache;

import com.doisdoissete.data.cache.base.BaseCache;
import com.doisdoissete.data.model.CareerEntity;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by broto on 9/23/15.
 */
public class CareerCache extends BaseCache<CareerEntity> {

    @Inject
    public CareerCache(Realm realm) {
        super(realm);
    }

    @Override
    public Class<CareerEntity> getReferenceClass() {
        return CareerEntity.class;
    }
}

