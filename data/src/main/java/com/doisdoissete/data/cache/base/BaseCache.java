package com.doisdoissete.data.cache.base;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by broto on 11/3/15.
 */
public abstract class BaseCache<T extends RealmObject> implements Cache<T>{

    private Realm realm;

    public BaseCache(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void put(T t) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(t);
        realm.commitTransaction();

        updateExpireTime(System.currentTimeMillis());
    }

    @Override
    public void putAll(List<T> t) {
        realm.beginTransaction();

        for (T item : t) {
            realm.copyToRealmOrUpdate(item);
        }

        realm.commitTransaction();
    }

    @Override
    public void delete(final int id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(getReferenceClass()).equalTo("id", id).findFirst().removeFromRealm();
            }
        });
    }

    @Override
    public void deleteAll() {
        realm.beginTransaction();
        realm.where(getReferenceClass()).findAll().clear();
        realm.commitTransaction();
    }

    @Override
    public T get(int id) {
        return realm.where(getReferenceClass()).equalTo("id", id).findFirst();
    }

    @Override
    public List<T> getAll() {
        return realm.where(getReferenceClass()).findAll();
    }

    public boolean isExpired() {
        RealmQuery<Sync> query = realm.where(Sync.class);
        query.equalTo("className", getReferenceClass().getName());
        Sync result = query.findFirst();

        if (result == null) {
            return true;
        }

        Date expiredDate = new Date(result.getLastUpdated() + getCacheLifeTime());

        return DateTime.now().isAfter(expiredDate.getTime());
    }

    private void updateExpireTime(long timestamp) {
        Sync sync = new Sync(getReferenceClass().getName(), timestamp);

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(sync);
        realm.commitTransaction();
    }

    protected long getCacheLifeTime() {
        return Sync.DEFAULT_CACHE_TIME_LIMIT;
    }

    public abstract Class<T> getReferenceClass();

}
