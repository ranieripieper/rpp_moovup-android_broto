package com.doisdoissete.data.cache;

import com.doisdoissete.data.cache.base.BaseCache;
import com.doisdoissete.data.model.CategoryEntity;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by broto on 11/3/15.
 */
public class CategoryCache extends BaseCache<CategoryEntity> {

    @Inject
    public CategoryCache(Realm realm) {
        super(realm);
    }

    @Override
    public Class<CategoryEntity> getReferenceClass() {
        return CategoryEntity.class;
    }
}
