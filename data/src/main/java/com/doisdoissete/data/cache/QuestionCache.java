package com.doisdoissete.data.cache;

import com.doisdoissete.data.cache.base.BaseCache;
import com.doisdoissete.data.model.QuestionEntity;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by broto on 11/5/15.
 */
public class QuestionCache extends BaseCache<QuestionEntity> {

    @Inject
    public QuestionCache(Realm realm) {
        super(realm);
    }

    @Override
    public Class<QuestionEntity> getReferenceClass() {
        return QuestionEntity.class;
    }
}
