package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.model.HobbiesResponse;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.events.OnHobbiesArrivedEvent;
import com.doisdoissete.domain.events.OnHobbiesErrorEvent;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 9/23/15.
 */
public class GetHobbiesUseCase extends UseCase{

    protected RestApi restApi;

    @Inject
    public GetHobbiesUseCase(EventBus bus, @Named("PublicAPI") RestApi restApi) {
        super(bus);
        this.restApi = restApi;
    }

    public void getAllHobbies() {
        restApi.getApiService().getAllHobbies(new Callback<HobbiesResponse>() {
            @Override
            public void success(HobbiesResponse hobbiesResponse, Response response) {
                bus.post(new OnHobbiesArrivedEvent(hobbiesResponse.getHobbyEntityList()));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new OnHobbiesErrorEvent());
            }
        });
    }
}