package com.doisdoissete.domain.interactor;

import android.util.Log;
import android.webkit.MimeTypeMap;

import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.net.RestApi;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Lazy;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by broto on 10/5/15.
 */
public class UploadProfilePhotoUseCase extends UseCase {

    private Lazy<RestApi> defaultApi;

    @Inject
    public UploadProfilePhotoUseCase(EventBus bus,
                                     @Named("AuthenticatedAPI") Lazy<RestApi> defaultApi) {
        super(bus);
        this.defaultApi = defaultApi;
    }

    public void uploadProfilePhoto(String photoPath) {
        defaultApi.get().getApiService().updatePhoto(
                new TypedFile(getMimeType(photoPath), new File(photoPath)),
                new TypedString(photoPath.substring(photoPath.lastIndexOf("/"), photoPath.length())),
                new Callback<ResponseResult>() {
            @Override
            public void success(ResponseResult responseResult, Response response) {
                Log.d("", "");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("", "");
            }
        });
    }

    private String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
