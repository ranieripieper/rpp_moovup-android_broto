package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.cache.CategoryCache;
import com.doisdoissete.data.cache.QuestionCache;
import com.doisdoissete.data.cache.UserCacheNew;
import com.doisdoissete.data.model.CategoryEntity;
import com.doisdoissete.data.model.FeedEntity;
import com.doisdoissete.data.model.QuestionEntity;
import com.doisdoissete.data.model.UserEntityNew;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.domain.entity.Question;
import com.doisdoissete.domain.entity.User;
import com.doisdoissete.domain.entity.UserNew;
import com.doisdoissete.domain.entity.mapper.CategoryMapper;
import com.doisdoissete.domain.entity.mapper.QuestionMapper;
import com.doisdoissete.domain.entity.mapper.UserMapperNew;
import com.doisdoissete.domain.events.OnFeedErrorEvent;
import com.doisdoissete.domain.events.OnFeedSuccessEvent;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 11/5/15.
 */
public class GetFeedUseCase extends UseCase {

    private QuestionCache questionCache;
    private QuestionMapper questionMapper;
    private UserCacheNew userCache;
    private UserMapperNew userMapper;
    private CategoryCache categoryCache;
    private CategoryMapper categoryMapper;
    private RestApi restApi;

    @Inject
    public GetFeedUseCase(EventBus bus,
                          QuestionCache questionCache,
                          QuestionMapper questionMapper,
                          UserCacheNew userCache,
                          UserMapperNew userMapper,
                          CategoryCache categoryCache,
                          CategoryMapper categoryMapper,
                          @Named("AuthenticatedAPI") RestApi restApi) {
        super(bus);
        this.questionCache = questionCache;
        this.questionMapper = questionMapper;
        this.userCache = userCache;
        this.userMapper = userMapper;
        this.categoryCache = categoryCache;
        this.categoryMapper = categoryMapper;
        this.restApi = restApi;
    }

    public void getFeed() {
        restApi.getApiService().getNewsFeed(new Callback<FeedEntity>() {
            @Override
            public void success(FeedEntity feedEntity, Response response) {
                List<QuestionEntity> questions = new ArrayList<>();
                List<UserEntityNew> users = new ArrayList<>();
                List<CategoryEntity> categories = new ArrayList<>();

                if (feedEntity.getLinkedData() != null) {
                    if (feedEntity.getLinkedData().getUsers() != null) {
                        users = feedEntity.getLinkedData().getUsers();
                        userCache.putAll(users);
                    }

                    if (feedEntity.getLinkedData().getCategories() != null) {
                        categories = feedEntity.getLinkedData().getCategories();
                        categoryCache.putAll(categories);
                    }
                }

                if (feedEntity.getQuestions() != null) {
                    questions = feedEntity.getQuestions();
                    questionCache.putAll(questions);
                }

                bus.post(new OnFeedSuccessEvent(questionMapper.map(questions),
                        userMapper.map(users),
                        categoryMapper.map(categories)
                ));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new OnFeedErrorEvent());
            }
        });
    }
}
