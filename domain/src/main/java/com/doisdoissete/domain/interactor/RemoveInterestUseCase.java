package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.net.RestApi;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 11/4/15.
 */
public class RemoveInterestUseCase extends UseCase {

    private RestApi defaultApi;

    @Inject
    public RemoveInterestUseCase(EventBus bus, @Named("AuthenticatedAPI") RestApi defaultApi) {
        super(bus);
        this.defaultApi = defaultApi;
    }

    public void remove(int categoryId) {
        defaultApi.getApiService().removeInterest(categoryId, new Callback<ResponseResult>() {
            @Override
            public void success(ResponseResult responseResult, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
