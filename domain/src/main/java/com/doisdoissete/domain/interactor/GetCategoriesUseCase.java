package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.cache.CategoryCache;
import com.doisdoissete.data.model.CategoryEntity;
import com.doisdoissete.data.model.CategoryResponse;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.domain.entity.mapper.CategoryMapper;
import com.doisdoissete.domain.events.OnCategoriesErrorEvent;
import com.doisdoissete.domain.events.OnCategoriesSuccessEvent;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 9/23/15.
 */
public class GetCategoriesUseCase extends UseCase{

    private RestApi restApi;
    private CategoryMapper mapper;
    private CategoryCache cache;

    @Inject
    public GetCategoriesUseCase(EventBus bus,
                                CategoryMapper mapper,
                                CategoryCache cache,
                                @Named("AuthenticatedAPI") RestApi restApi) {
        super(bus);
        this.restApi = restApi;
        this.mapper = mapper;
        this.cache = cache;
    }

    public void getCategories() {

        if (!cache.isExpired()) {
            bus.post(new OnCategoriesSuccessEvent(mapper.map(cache.getAll())));
            return;
        }

        restApi.getApiService().getCategories(new Callback<CategoryResponse>() {
            @Override
            public void success(CategoryResponse categoryResponse, Response response) {
                List<Category> categoryList = mapper.map(categoryResponse.getCategoryEntityList());

                cache.putAll(categoryResponse.getCategoryEntityList());
                bus.post(new OnCategoriesSuccessEvent(categoryList));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new OnCategoriesErrorEvent());
            }
        });
    }
}
