package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.cache.UserCache;
import com.doisdoissete.data.model.LoginEntity;
import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.model.UserEntity;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.entity.User;
import com.doisdoissete.domain.events.OnLoginErrorEvent;
import com.doisdoissete.domain.events.OnLoginSuccessEvent;

import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 9/21/15.
 */
public class LoginEmailUseCase extends UseCase {

    protected RestApi restApi;
    protected UserCache cache;

    private String login, password;

    @Inject
    public LoginEmailUseCase(EventBus bus, @Named("PublicAPI") RestApi restApi, UserCache cache) {
        super(bus);
        this.restApi = restApi;
        this.cache = cache;
    }

    public void login(String login, String password) {
        this.login = login;
        this.password = password;

        if (login.isEmpty() || password.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            restApi.
                    getApiService().
                    login(new LoginEntity(getLogin(), getPassword()), new Callback<ResponseResult>() {
                        @Override
                        public void success(ResponseResult responseResult, Response response) {
                            User user = new User();
                            user.setAuthToken(responseResult.getAuthData().getAuthToken());
                            user.setExpiresAt(responseResult.getAuthData().getExpiresAt());
                            user.setProfileImageUrl(responseResult.getUserData().getProfileImageUrl());
                            user.setUsername(responseResult.getUserData().getUsername());
                            user.setNewUser(responseResult.isNewUser());

                            ModelMapper modelMapper = new ModelMapper();

                            cache.put(modelMapper.map(user, UserEntity.class));
                            bus.post(new OnLoginSuccessEvent(user));
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            bus.post(new OnLoginErrorEvent());
                        }
                    });
        }
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
