package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.model.CareerResponse;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.events.OnCareersArrivedEvent;
import com.doisdoissete.domain.events.OnCareersErrorEvent;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 9/23/15.
 */
public class GetCareersUseCase extends UseCase{

    protected RestApi restApi;

    @Inject
    public GetCareersUseCase(EventBus bus, @Named("PublicAPI") RestApi restApi) {
        super(bus);
        this.restApi = restApi;
    }

    public void getCareers(int perPage) {
        restApi.getApiService().getCareers(perPage, new Callback<CareerResponse>() {
            @Override
            public void success(CareerResponse careerResponse, Response response) {
                bus.post(new OnCareersArrivedEvent(careerResponse.getCarrerEntityList()));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new OnCareersErrorEvent());
            }
        });
    }
}
