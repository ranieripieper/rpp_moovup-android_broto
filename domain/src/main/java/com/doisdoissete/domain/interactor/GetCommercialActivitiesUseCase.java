package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.model.CommercialActivityResponse;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.events.OnCommercialActivityErrorEvent;
import com.doisdoissete.domain.events.OnCommercialActivitySuccessEvent;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 10/10/15.
 */
public class GetCommercialActivitiesUseCase extends UseCase{

    protected RestApi restApi;

    @Inject
    public GetCommercialActivitiesUseCase(EventBus bus, @Named("PublicAPI") RestApi restApi) {
        super(bus);
        this.restApi = restApi;
    }

    public void getAllComercialActivities() {
        restApi.getApiService().getAllCommercialActivities(new Callback<CommercialActivityResponse>() {
            @Override
            public void success(CommercialActivityResponse commercialActivityResponse, Response response) {
                bus.post(new OnCommercialActivitySuccessEvent(commercialActivityResponse.getCommercialActivityEntityList()));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new OnCommercialActivityErrorEvent());
            }
        });
    }
}