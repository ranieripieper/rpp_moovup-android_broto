package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.model.GoalEntity;
import com.doisdoissete.data.model.GoalRequest;
import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.events.OnGoalAddErrorEvent;
import com.doisdoissete.domain.events.OnGoalAddSuccessEvent;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 10/16/15.
 */
public class NewGoalUseCase extends UseCase {

    private RestApi restApi;

    @Inject
    public NewGoalUseCase(EventBus bus, @Named("AuthenticatedAPI") RestApi restApi) {
        super(bus);
        this.restApi = restApi;
    }

    public void addGoal(GoalEntity goal) {
        restApi.getApiService().addGoal(new GoalRequest(goal), new Callback<ResponseResult>() {
            @Override
            public void success(ResponseResult responseResult, Response response) {
                bus.post(new OnGoalAddSuccessEvent());
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new OnGoalAddErrorEvent());
            }
        });
    }


}
