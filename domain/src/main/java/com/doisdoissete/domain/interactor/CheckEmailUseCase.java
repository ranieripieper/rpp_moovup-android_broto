package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.model.CareerResponse;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.events.OnEmailDoesntExistEvent;
import com.doisdoissete.domain.events.OnEmailExistEvent;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 10/13/15.
 */
public class CheckEmailUseCase extends UseCase {

    protected RestApi restApi;

    @Inject
    public CheckEmailUseCase(EventBus bus, @Named("PublicAPI") RestApi restApi) {
        super(bus);
        this.restApi = restApi;
    }

    public void checkEmail(String email) {
        restApi.getApiService().checkEmail(email, new Callback<CareerResponse>() {
            @Override
            public void success(CareerResponse careerResponse, Response response) {
                bus.post(new OnEmailExistEvent());
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new OnEmailDoesntExistEvent());
            }
        });
    }
}