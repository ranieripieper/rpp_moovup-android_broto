package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.cache.TokenCache;
import com.doisdoissete.domain.entity.User;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 10/13/15.
 */
public class GetLoggedUserUseCase extends UseCase{

    private TokenCache cache;

    @Inject
    public GetLoggedUserUseCase(EventBus bus, TokenCache cache) {
        super(bus);
        this.cache = cache;
    }

    public User getuser(){
        if (!cache.get(0).isEmpty()) {
            return new User();
        }

        return null;
    }
}
