package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.model.EmailEntity;
import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.events.OnForgotPasswordErrorEvent;
import com.doisdoissete.domain.events.OnForgotPasswordSuccessEvent;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 9/24/15.
 */
public class ForgotPasswordUseCase extends UseCase {

    protected RestApi restApi;

    @Inject
    public ForgotPasswordUseCase(EventBus bus, @Named("PublicAPI") RestApi restApi) {
        super(bus);
        this.restApi = restApi;
    }

    public void requestPassword(String email) {
        restApi.getApiService().requestRecover(new EmailEntity(email), new Callback<ResponseResult>() {
            @Override
            public void success(ResponseResult responseResult, Response response) {
                bus.post(new OnForgotPasswordSuccessEvent());
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new OnForgotPasswordErrorEvent());
            }
        });
    }
}
