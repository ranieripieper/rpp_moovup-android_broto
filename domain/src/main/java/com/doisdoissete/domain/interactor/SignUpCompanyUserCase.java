package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.cache.UserCache;
import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.model.SignUpEntity;
import com.doisdoissete.data.model.UserEntity;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.entity.Company;
import com.doisdoissete.domain.entity.User;
import com.doisdoissete.domain.entity.mapper.CompanyMapper;
import com.doisdoissete.domain.events.OnSignUpErrorEvent;
import com.doisdoissete.domain.events.OnSignUpSuccessEvent;

import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 10/1/15.
 */
public class SignUpCompanyUserCase extends UseCase {

    private CompanyMapper mapper;
    private RestApi restApi;
    private UserCache cache;
    private UploadProfilePhotoUseCase uploadProfilePhotoUseCase;

    @Inject
    public SignUpCompanyUserCase(EventBus bus, @Named("PublicAPI") RestApi restApi,
                                 UserCache cache,
                                 CompanyMapper mapper,
                                 UploadProfilePhotoUseCase uploadProfilePhotoUseCase) {
        super(bus);
        this.restApi = restApi;
        this.cache = cache;
        this.mapper = mapper;
        this.uploadProfilePhotoUseCase = uploadProfilePhotoUseCase;
    }

    public void signUp(final Company company) {
        restApi.getApiService().signUpCompany(
                new SignUpEntity<>(mapper.map(company)),
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        User user = new User();
                        user.setAuthToken(responseResult.getAuthData().getAuthToken());
                        user.setExpiresAt(responseResult.getAuthData().getExpiresAt());
                        user.setProfileImageUrl(responseResult.getUserData().getProfileImageUrl());
                        user.setUsername(responseResult.getUserData().getUsername());
                        user.setNewUser(responseResult.isNewUser());

                        ModelMapper modelMapper = new ModelMapper();

                        cache.put(modelMapper.map(user, UserEntity.class));

                        if (company.getPhotoPath() != null && !company.getPhotoPath().isEmpty()) {
                            uploadProfilePhotoUseCase.uploadProfilePhoto(company.getPhotoPath());
                        }

                        bus.post(new OnSignUpSuccessEvent());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (error != null) {
                            try {
                                ResponseResult errorObject = (ResponseResult) error.getBodyAs(ResponseResult.class);

                                bus.post(new OnSignUpErrorEvent(errorObject.getErrors()));
                            } catch (Exception e) {
                                bus.post(new OnSignUpErrorEvent());
                            }
                        } else {
                            bus.post(new OnSignUpErrorEvent());
                        }
                    }
                });
    }
}
