package com.doisdoissete.domain.interactor;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 8/27/15.
 */
public abstract class UseCase {

    protected EventBus bus;

    public UseCase(EventBus bus) {
        this.bus = bus;
    }
}
