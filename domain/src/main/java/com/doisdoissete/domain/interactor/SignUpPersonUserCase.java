package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.cache.UserCache;
import com.doisdoissete.data.model.PersonEntity;
import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.model.SignUpEntity;
import com.doisdoissete.data.model.UserEntity;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.entity.Person;
import com.doisdoissete.domain.entity.User;
import com.doisdoissete.domain.entity.mapper.PersonMapper;
import com.doisdoissete.domain.events.OnSignUpErrorEvent;
import com.doisdoissete.domain.events.OnSignUpSuccessEvent;

import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 9/22/15.
 */
public class SignUpPersonUserCase extends UseCase {

    private RestApi loginApi;
    private UserCache cache;
    private PersonMapper mapper;
    private UploadProfilePhotoUseCase uploadProfilePhotoUseCase;


    @Inject
    public SignUpPersonUserCase(EventBus bus,
                                @Named("PublicAPI") RestApi loginApi,
                                UserCache cache,
                                PersonMapper mapper,
                                UploadProfilePhotoUseCase uploadProfilePhotoUseCase) {
        super(bus);
        this.loginApi = loginApi;
        this.cache = cache;
        this.mapper = mapper;
        this.uploadProfilePhotoUseCase = uploadProfilePhotoUseCase;
    }

    public void signUp(final Person person) {
        PersonEntity entity = mapper.map(person);

        loginApi.getApiService().signUpPerson(
                new SignUpEntity<>(entity),
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        User user = new User();
                        user.setAuthToken(responseResult.getAuthData().getAuthToken());
                        user.setExpiresAt(responseResult.getAuthData().getExpiresAt());
                        user.setProfileImageUrl(responseResult.getUserData().getProfileImageUrl());
                        user.setUsername(responseResult.getUserData().getUsername());
                        user.setNewUser(responseResult.isNewUser());

                        ModelMapper modelMapper = new ModelMapper();

                        cache.put(modelMapper.map(user, UserEntity.class));

                        if (person.getPhotoPath() != null && !person.getPhotoPath().isEmpty()) {
                            uploadProfilePhotoUseCase.uploadProfilePhoto(person.getPhotoPath());
                        }

                        bus.post(new OnSignUpSuccessEvent());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (error != null) {
                            try {
                                ResponseResult errorObject = (ResponseResult) error.getBodyAs(ResponseResult.class);

                                bus.post(new OnSignUpErrorEvent(errorObject.getErrors()));
                            } catch (Exception e) {
                                bus.post(new OnSignUpErrorEvent());
                            }
                        } else {
                            bus.post(new OnSignUpErrorEvent());
                        }
                    }
                });
    }
}
