package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.cache.UserCache;
import com.doisdoissete.data.model.ResponseResult;
import com.doisdoissete.data.model.SocialLoginEntity;
import com.doisdoissete.data.model.UserEntity;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.domain.entity.User;
import com.doisdoissete.domain.events.OnSocialLoginErrorEvent;
import com.doisdoissete.domain.events.OnSocialLoginSuccessEvent;

import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.inject.Named;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 9/28/15.
 */
public class LoginGoogleUseCase extends UseCase {

    RestApi restApi;
    UserCache cache;

    @Inject
    public LoginGoogleUseCase(EventBus bus, @Named("PublicAPI") RestApi restApi, UserCache cache) {
        super(bus);
        this.restApi = restApi;
        this.cache = cache;
    }

    public void login(String token) {
        restApi.getApiService().loginGoogle(new SocialLoginEntity(token), new Callback<ResponseResult>() {
            @Override
            public void success(ResponseResult responseResult, Response response) {
                User user = new User();
                user.setAuthToken(responseResult.getAuthData().getAuthToken());
                user.setExpiresAt(responseResult.getAuthData().getExpiresAt());
                user.setProfileImageUrl(responseResult.getUserData().getProfileImageUrl());
                user.setUsername(responseResult.getUserData().getUsername());
                user.setNewUser(responseResult.isNewUser());

                ModelMapper modelMapper = new ModelMapper();

                cache.put(modelMapper.map(user, UserEntity.class));
                bus.post(new OnSocialLoginSuccessEvent(user));
            }

            @Override
            public void failure(RetrofitError error) {
                if (error != null) {
                    try {
                        ResponseResult errorObject = (ResponseResult) error.getBodyAs(ResponseResult.class);

                        bus.post(new OnSocialLoginErrorEvent(errorObject.getErrors()));
                    } catch (Exception e) {
                        bus.post(new OnSocialLoginErrorEvent());
                    }
                } else {
                    bus.post(new OnSocialLoginErrorEvent());
                }
            }
        });
    }
}
