package com.doisdoissete.domain.interactor;

import com.doisdoissete.data.cache.TokenCache;
import com.doisdoissete.domain.entity.User;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 11/3/15.
 */
public class GetSettingsUseCase extends UseCase{

    private TokenCache cache;

    @Inject
    public GetSettingsUseCase(EventBus bus, TokenCache cache) {
        super(bus);
        this.cache = cache;
    }

    public User getuser(){
        if (!cache.get(0).isEmpty()) {
            return new User();
        }

        return null;
    }
}
