package com.doisdoissete.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by broto on 11/5/15.
 */
public class Question implements Parcelable {

    private int id;
    private int userId;
    private String bodyText;
    private String upvotesCount;
    private Date createdAt;
    private int favoritedCount;
    private int status;
    private int categoryId;
    private UserNew user;
    private Category category;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public String getUpvotesCount() {
        return upvotesCount;
    }

    public void setUpvotesCount(String upvotesCount) {
        this.upvotesCount = upvotesCount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getFavoritedCount() {
        return favoritedCount;
    }

    public void setFavoritedCount(int favoritedCount) {
        this.favoritedCount = favoritedCount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public UserNew getUser() {
        return user;
    }

    public void setUser(UserNew user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.userId);
        dest.writeString(this.bodyText);
        dest.writeString(this.upvotesCount);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
        dest.writeInt(this.favoritedCount);
        dest.writeInt(this.status);
        dest.writeInt(this.categoryId);
        dest.writeParcelable(this.user, flags);
        dest.writeParcelable(this.category, 0);
    }

    public Question() {
    }

    protected Question(Parcel in) {
        this.id = in.readInt();
        this.userId = in.readInt();
        this.bodyText = in.readString();
        this.upvotesCount = in.readString();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        this.favoritedCount = in.readInt();
        this.status = in.readInt();
        this.categoryId = in.readInt();
        this.user = in.readParcelable(UserNew.class.getClassLoader());
        this.category = in.readParcelable(Category.class.getClassLoader());
    }

    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>() {
        public Question createFromParcel(Parcel source) {
            return new Question(source);
        }

        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
}
