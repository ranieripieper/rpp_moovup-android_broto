package com.doisdoissete.domain.entity;

/**
 * Created by broto on 9/22/15.
 */
public class Person {

    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private String password;
    private String passwordConfirmation;
    private int hobbyId;
    private int carrerId;
    private String photoPath;
    private String birthday;

    public Person() {
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public Person(String firstName, String lastName, String email, String gender, String password, int hobbyId, int carrerId, String photoPath, String birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.gender = gender;
        this.password = password;

        this.hobbyId = hobbyId;
        this.carrerId = carrerId;
        this.photoPath = photoPath;
        this.birthday = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getHobbyId() {
        return hobbyId;
    }

    public void setHobbyId(int hobbyId) {
        this.hobbyId = hobbyId;
    }

    public int getCarrerId() {
        return carrerId;
    }

    public void setCarrerId(int carrerId) {
        this.carrerId = carrerId;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
