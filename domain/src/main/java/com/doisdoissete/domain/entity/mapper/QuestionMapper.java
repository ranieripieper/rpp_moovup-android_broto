package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.QuestionEntity;
import com.doisdoissete.domain.entity.Question;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 9/23/15.
 */
public class QuestionMapper {

    @Inject
    public QuestionMapper() {
    }

    public List<Question> map(List<QuestionEntity> origin) {
        List<Question> hobbies = new ArrayList<>(origin.size());

        for (QuestionEntity questionEntity : origin) {
            hobbies.add(map(questionEntity));
        }

        return hobbies;
    }

    public Question map(QuestionEntity origin) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(origin, Question.class);
    }
}
