package com.doisdoissete.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by broto on 11/5/15.
 */
public class ProfileImage implements Parcelable {

    private String thumb;
    private String medium;

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.thumb);
        dest.writeString(this.medium);
    }

    public ProfileImage() {
    }

    protected ProfileImage(Parcel in) {
        this.thumb = in.readString();
        this.medium = in.readString();
    }

    public static final Parcelable.Creator<ProfileImage> CREATOR = new Parcelable.Creator<ProfileImage>() {
        public ProfileImage createFromParcel(Parcel source) {
            return new ProfileImage(source);
        }

        public ProfileImage[] newArray(int size) {
            return new ProfileImage[size];
        }
    };
}
