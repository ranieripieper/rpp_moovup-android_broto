package com.doisdoissete.domain.entity;

/**
 * Created by broto on 10/1/15.
 */
public class Company {

    String email;
    String password;
    String passwordConfirmation;
    String photoPath;
    String companyName;
    String foundationDate;
    int commercialActivityId;

    public Company(String email, String password, String passwordConfirmation, String photoPath, String companyName, String foundationDate, int commercialActivityId) {
        this.email = email;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.photoPath = photoPath;
        this.companyName = companyName;
        this.foundationDate = foundationDate;
        this.commercialActivityId = commercialActivityId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(String foundationDate) {
        this.foundationDate = foundationDate;
    }

    public int getCommercialActivityId() {
        return commercialActivityId;
    }

    public void setCommercialActivityId(int commercialActivityId) {
        this.commercialActivityId = commercialActivityId;
    }
}
