package com.doisdoissete.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.doisdoissete.data.model.ProfileImageEntity;

import java.util.Date;

/**
 * Created by broto on 11/5/15.
 */
public class UserNew implements Parcelable {

    private int id;
    private String profileType;
    private String firstName;
    private String lastName;
    private String userName;
    private String fullName;
    private String profileImageUrl;
    private ProfileImage profileImage;
    private Date createdAt;
    private Date updatedAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public ProfileImage getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.profileType);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.userName);
        dest.writeString(this.fullName);
        dest.writeString(this.profileImageUrl);
        dest.writeParcelable(this.profileImage, flags);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
        dest.writeLong(updatedAt != null ? updatedAt.getTime() : -1);
    }

    public UserNew() {
    }

    protected UserNew(Parcel in) {
        this.id = in.readInt();
        this.profileType = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.userName = in.readString();
        this.fullName = in.readString();
        this.profileImageUrl = in.readString();
        this.profileImage = in.readParcelable(ProfileImage.class.getClassLoader());
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
    }

    public static final Parcelable.Creator<UserNew> CREATOR = new Parcelable.Creator<UserNew>() {
        public UserNew createFromParcel(Parcel source) {
            return new UserNew(source);
        }

        public UserNew[] newArray(int size) {
            return new UserNew[size];
        }
    };
}
