package com.doisdoissete.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by broto on 10/13/15.
 */
public class Category implements Parcelable {

    private int id;
    private int position;
    private String title;
    private String measurementType;
    private List<Category> children;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.position);
        dest.writeString(this.title);
        dest.writeString(this.measurementType);
        dest.writeList(this.children);
    }

    public Category() {
    }

    protected Category(Parcel in) {
        this.id = in.readInt();
        this.position = in.readInt();
        this.title = in.readString();
        this.measurementType = in.readString();
        this.children = new ArrayList<Category>();
        in.readList(this.children, List.class.getClassLoader());
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
