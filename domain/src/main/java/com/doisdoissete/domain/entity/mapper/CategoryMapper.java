package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.CategoryEntity;
import com.doisdoissete.domain.entity.Category;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 10/14/15.
 */
public class CategoryMapper {

    @Inject
    public CategoryMapper() {
    }

    public List<Category> map(List<CategoryEntity> origin) {
        List<Category> categories = new ArrayList<>(origin.size());

        for (CategoryEntity categoryEntity : origin) {
            categories.add(map(categoryEntity));
        }

        return categories;
    }

    public Category map(CategoryEntity origin) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(origin, Category.class);
    }
}
