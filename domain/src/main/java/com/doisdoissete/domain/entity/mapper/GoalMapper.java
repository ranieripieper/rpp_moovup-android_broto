package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.GoalEntity;
import com.doisdoissete.domain.entity.Goal;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 10/14/15.
 */
public class GoalMapper {

    @Inject
    public GoalMapper() {
    }

    public GoalEntity map(Goal origin) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(origin, GoalEntity.class);
    }
}
