package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.CareerEntity;
import com.doisdoissete.domain.entity.Career;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 9/23/15.
 */
public class CareersMapper {

    @Inject
    public CareersMapper() {
    }

    public List<Career> map(List<CareerEntity> origin) {
        List<Career> careers = new ArrayList<>(origin.size());

        for (CareerEntity careerEntity : origin) {
            careers.add(map(careerEntity));
        }

        return careers;
    }

    public Career map(CareerEntity origin) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(origin, Career.class);
    }
}
