package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.CommercialActivityEntity;
import com.doisdoissete.domain.entity.CommercialActivity;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 10/10/15.
 */
public class CommercialActivityMapper {

    @Inject
    public CommercialActivityMapper() {
    }

    public List<CommercialActivity> map(List<CommercialActivityEntity> origin) {
        List<CommercialActivity> commercialActivityList = new ArrayList<>(origin.size());

        for (CommercialActivityEntity commercialActivityEntity : origin) {
            commercialActivityList.add(map(commercialActivityEntity));
        }

        return commercialActivityList;
    }

    public CommercialActivity map(CommercialActivityEntity origin) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(origin, CommercialActivity.class);
    }
}
