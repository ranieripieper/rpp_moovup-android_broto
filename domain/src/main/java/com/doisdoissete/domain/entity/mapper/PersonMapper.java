package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.PersonEntity;
import com.doisdoissete.domain.entity.Person;

import javax.inject.Inject;

/**
 * Created by broto on 10/1/15.
 */
public class PersonMapper {

    @Inject
    public PersonMapper() {
    }

    public PersonEntity map(Person person) {
        PersonEntity entity = new PersonEntity(person.getFirstName(),
                person.getLastName(),
                person.getEmail(),
                person.getGender(),
                person.getPassword(),
                person.getPasswordConfirmation(),
                person.getHobbyId(),
                person.getCarrerId());

        return entity;
    }
}
