package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.ProfileImageEntity;
import com.doisdoissete.data.model.UserEntityNew;
import com.doisdoissete.domain.entity.ProfileImage;
import com.doisdoissete.domain.entity.UserNew;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 11/5/15.
 */
public class UserMapperNew {

    @Inject
    public UserMapperNew() {
    }

    public List<UserNew> map(List<UserEntityNew> origin) {
        List<UserNew> userList = new ArrayList<>(origin.size());

        for (UserEntityNew userEntityNew : origin) {
            userList.add(map(userEntityNew));
        }

        return userList;
    }

    public UserNew map(UserEntityNew origin) {
        ModelMapper modelMapper = new ModelMapper();

        UserNew user = modelMapper.map(origin, UserNew.class);
        user.setProfileImage(map(origin.getProfileImage()));

        return user;
    }

    public ProfileImage map(ProfileImageEntity origin) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(origin, ProfileImage.class);
    }
}
