package com.doisdoissete.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by broto on 10/16/15.
 */
public class Goal implements Parcelable {

    private String title;
    private String periodicityType;
    private String targetDate;
    private int totalMinutes;
    private boolean mPublic;
    private float currentWeight;
    private float targetWeight;
    private int categoryId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPeriodicityType() {
        return periodicityType;
    }

    public void setPeriodicityType(String periodicityType) {
        this.periodicityType = periodicityType;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public int getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(int totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public boolean ismPublic() {
        return mPublic;
    }

    public void setmPublic(boolean mPublic) {
        this.mPublic = mPublic;
    }

    public float getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(float currentWeight) {
        this.currentWeight = currentWeight;
    }

    public float getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(float targetWeight) {
        this.targetWeight = targetWeight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.periodicityType);
        dest.writeString(this.targetDate);
        dest.writeInt(this.totalMinutes);
        dest.writeByte(mPublic ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.currentWeight);
        dest.writeFloat(this.targetWeight);
        dest.writeInt(this.categoryId);
    }

    public Goal() {
    }

    protected Goal(Parcel in) {
        this.title = in.readString();
        this.periodicityType = in.readString();
        this.targetDate = in.readString();
        this.totalMinutes = in.readInt();
        this.mPublic = in.readByte() != 0;
        this.currentWeight = in.readFloat();
        this.targetWeight = in.readFloat();
        this.categoryId = in.readInt();
    }

    public static final Parcelable.Creator<Goal> CREATOR = new Parcelable.Creator<Goal>() {
        public Goal createFromParcel(Parcel source) {
            return new Goal(source);
        }

        public Goal[] newArray(int size) {
            return new Goal[size];
        }
    };
}
