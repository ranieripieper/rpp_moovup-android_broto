package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.InterestEntity;
import com.doisdoissete.domain.entity.Interest;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 10/14/15.
 */
public class InterestMapper {

    @Inject
    public InterestMapper() {
    }

    public InterestEntity map(Interest origin) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(origin, InterestEntity.class);
    }
}
