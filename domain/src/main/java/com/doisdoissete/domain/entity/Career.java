package com.doisdoissete.domain.entity;

/**
 * Created by broto on 9/23/15.
 */
public class Career {

    private int id;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
