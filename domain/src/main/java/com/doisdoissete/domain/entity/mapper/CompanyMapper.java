package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.CompanyEntity;
import com.doisdoissete.domain.entity.Company;

import javax.inject.Inject;

/**
 * Created by broto on 10/1/15.
 */
public class CompanyMapper {

    @Inject
    public CompanyMapper() {
    }

    public CompanyEntity map(Company company) {
        CompanyEntity entity = new CompanyEntity(company.getCompanyName(),
                company.getFoundationDate(),
                company.getCommercialActivityId(),
                company.getEmail(),
                company.getPassword(),
                company.getPasswordConfirmation());
        return entity;
    }
}
