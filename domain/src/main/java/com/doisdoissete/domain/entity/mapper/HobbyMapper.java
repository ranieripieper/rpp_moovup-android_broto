package com.doisdoissete.domain.entity.mapper;

import com.doisdoissete.data.model.HobbyEntity;
import com.doisdoissete.domain.entity.Hobby;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by broto on 9/23/15.
 */
public class HobbyMapper {

    @Inject
    public HobbyMapper() {
    }

    public List<Hobby> map(List<HobbyEntity> origin) {
        List<Hobby> hobbies = new ArrayList<>(origin.size());

        for (HobbyEntity hobbyEntity : origin) {
            hobbies.add(map(hobbyEntity));
        }

        return hobbies;
    }

    public Hobby map(HobbyEntity origin) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(origin, Hobby.class);
    }
}
