package com.doisdoissete.domain.events;

import com.doisdoissete.domain.entity.Category;

import java.util.List;

/**
 * Created by broto on 10/14/15.
 */
public class OnCategoriesSuccessEvent {

    private List<Category> categories;

    public OnCategoriesSuccessEvent(List<Category> categories) {
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
