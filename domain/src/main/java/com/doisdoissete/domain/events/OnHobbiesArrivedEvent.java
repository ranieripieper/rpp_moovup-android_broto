package com.doisdoissete.domain.events;

import com.doisdoissete.data.model.HobbyEntity;

import java.util.List;

/**
 * Created by broto on 9/23/15.
 */
public class OnHobbiesArrivedEvent {

    List<HobbyEntity> hobbyEntityList;

    public OnHobbiesArrivedEvent(List<HobbyEntity> hobbyEntityList) {
        this.hobbyEntityList = hobbyEntityList;
    }

    public List<HobbyEntity> getHobbyEntityList() {
        return hobbyEntityList;
    }

    public void setHobbyEntityList(List<HobbyEntity> hobbyEntityList) {
        this.hobbyEntityList = hobbyEntityList;
    }
}
