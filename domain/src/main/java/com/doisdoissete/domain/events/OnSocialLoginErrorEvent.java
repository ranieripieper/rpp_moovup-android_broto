package com.doisdoissete.domain.events;

import java.util.List;

/**
 * Created by broto on 9/22/15.
 */
public class OnSocialLoginErrorEvent {

    private List<String> errors;

    public OnSocialLoginErrorEvent() {
    }

    public OnSocialLoginErrorEvent(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
