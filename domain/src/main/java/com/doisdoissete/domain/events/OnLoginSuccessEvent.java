package com.doisdoissete.domain.events;

import com.doisdoissete.domain.entity.User;

/**
 * Created by broto on 9/22/15.
 */
public class OnLoginSuccessEvent {

    private User user;

    public OnLoginSuccessEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
