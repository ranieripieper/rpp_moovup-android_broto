package com.doisdoissete.domain.events;

import com.doisdoissete.data.model.CategoryEntity;
import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.domain.entity.Question;
import com.doisdoissete.domain.entity.UserNew;

import java.util.List;

/**
 * Created by broto on 11/5/15.
 */
public class OnFeedSuccessEvent {

    private List<Question> questionList;
    private List<UserNew> userList;
    private List<Category> categoryList;

    public OnFeedSuccessEvent(List<Question> questionList, List<UserNew> userList, List<Category> categoryList) {
        this.questionList = questionList;
        this.userList = userList;
        this.categoryList = categoryList;
    }

    public List<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }

    public List<UserNew> getUserList() {
        return userList;
    }

    public void setUserList(List<UserNew> userList) {
        this.userList = userList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
