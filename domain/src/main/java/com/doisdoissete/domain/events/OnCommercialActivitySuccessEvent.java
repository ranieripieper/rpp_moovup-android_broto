package com.doisdoissete.domain.events;

import com.doisdoissete.data.model.CommercialActivityEntity;

import java.util.List;

/**
 * Created by broto on 10/10/15.
 */
public class OnCommercialActivitySuccessEvent {

    List<CommercialActivityEntity> commercialActivityEntityList;

    public OnCommercialActivitySuccessEvent(List<CommercialActivityEntity> commercialActivityEntityList) {
        this.commercialActivityEntityList = commercialActivityEntityList;
    }

    public List<CommercialActivityEntity> getCommercialActivityEntityList() {
        return commercialActivityEntityList;
    }

    public void setCommercialActivityEntityList(List<CommercialActivityEntity> commercialActivityEntityList) {
        this.commercialActivityEntityList = commercialActivityEntityList;
    }
}
