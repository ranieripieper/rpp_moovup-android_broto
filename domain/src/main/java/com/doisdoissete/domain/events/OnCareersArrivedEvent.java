package com.doisdoissete.domain.events;

import com.doisdoissete.data.model.CareerEntity;

import java.util.List;

/**
 * Created by broto on 9/23/15.
 */
public class OnCareersArrivedEvent {

    List<CareerEntity> careerEntityList;

    public OnCareersArrivedEvent(List<CareerEntity> careerEntityList) {
        this.careerEntityList = careerEntityList;
    }

    public List<CareerEntity> getCarrerEntityList() {
        return careerEntityList;
    }

    public void setCarrerEntityList(List<CareerEntity> careerEntityList) {
        this.careerEntityList = careerEntityList;
    }
}
