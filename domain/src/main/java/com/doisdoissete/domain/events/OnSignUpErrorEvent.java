package com.doisdoissete.domain.events;

import java.util.List;

/**
 * Created by broto on 9/18/15.
 */
public class OnSignUpErrorEvent {

    private List<String> errors;

    public OnSignUpErrorEvent() {
    }

    public OnSignUpErrorEvent(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
