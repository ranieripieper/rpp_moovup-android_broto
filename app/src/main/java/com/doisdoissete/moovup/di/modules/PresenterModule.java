package com.doisdoissete.moovup.di.modules;

import com.doisdoissete.domain.entity.mapper.CareersMapper;
import com.doisdoissete.domain.entity.mapper.CommercialActivityMapper;
import com.doisdoissete.domain.entity.mapper.GoalMapper;
import com.doisdoissete.domain.entity.mapper.HobbyMapper;
import com.doisdoissete.domain.entity.mapper.InterestMapper;
import com.doisdoissete.domain.interactor.AddInterestUseCase;
import com.doisdoissete.domain.interactor.CheckEmailUseCase;
import com.doisdoissete.domain.interactor.ForgotPasswordUseCase;
import com.doisdoissete.domain.interactor.GetCareersUseCase;
import com.doisdoissete.domain.interactor.GetCategoriesUseCase;
import com.doisdoissete.domain.interactor.GetCommercialActivitiesUseCase;
import com.doisdoissete.domain.interactor.GetFeedUseCase;
import com.doisdoissete.domain.interactor.GetHobbiesUseCase;
import com.doisdoissete.domain.interactor.LoginEmailUseCase;
import com.doisdoissete.domain.interactor.LoginFacebookUseCase;
import com.doisdoissete.domain.interactor.LoginGoogleUseCase;
import com.doisdoissete.domain.interactor.NewGoalUseCase;
import com.doisdoissete.domain.interactor.RemoveInterestUseCase;
import com.doisdoissete.domain.interactor.SignUpCompanyUserCase;
import com.doisdoissete.domain.interactor.SignUpPersonUserCase;
import com.doisdoissete.moovup.presenters.FeedPresenter;
import com.doisdoissete.moovup.presenters.InterestPresenter;
import com.doisdoissete.moovup.presenters.GoalCategoriesPresenter;
import com.doisdoissete.moovup.presenters.GoalWeightPresenter;
import com.doisdoissete.moovup.presenters.LoginEmailPresenter;
import com.doisdoissete.moovup.presenters.LoginSignUpPresenter;
import com.doisdoissete.moovup.presenters.LoginSocialPresenter;
import com.doisdoissete.moovup.presenters.QuestionDetailPresenter;
import com.doisdoissete.moovup.presenters.SignUpCompanyPresenter;
import com.doisdoissete.moovup.presenters.SignUpPersonPresenter;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

/**
 * Created by broto on 9/23/15.
 */

@Module
public class PresenterModule {

    @Provides
    LoginEmailPresenter providesLoginPresenter(EventBus bus, LoginEmailUseCase loginEmailUseCase, ForgotPasswordUseCase forgotPasswordUseCase) {
        return new LoginEmailPresenter(bus, loginEmailUseCase, forgotPasswordUseCase);
    }

    @Provides
    LoginSocialPresenter providesLoginSocialNetworkPresenter(EventBus bus, LoginFacebookUseCase facebookUseCase, LoginGoogleUseCase googleUseCase) {
        return new LoginSocialPresenter(bus, facebookUseCase, googleUseCase);
    }

    @Provides
    SignUpPersonPresenter providesProfileInputPersonPresenter(EventBus bus,
                                                                    CareersMapper mapper,
                                                                    HobbyMapper hobbyMapper,
                                                                    SignUpPersonUserCase useCase,
                                                                    GetCareersUseCase carriersUseCase,
                                                                    GetHobbiesUseCase hobbiesUseCase) {
        return new SignUpPersonPresenter(bus,
                mapper,
                hobbyMapper,
                useCase,
                carriersUseCase,
                hobbiesUseCase);
    }

    @Provides
    SignUpCompanyPresenter providesProfileInputCompanyPresenter(
            EventBus bus, SignUpCompanyUserCase useCase, CommercialActivityMapper mapper, GetCommercialActivitiesUseCase commercialActivitiesUseCase) {
        return new SignUpCompanyPresenter(bus, useCase, mapper, commercialActivitiesUseCase);
    }

    @Provides
    LoginSignUpPresenter providesLoginSignUpPresenter(EventBus bus, CheckEmailUseCase emailUseCase) {
        return new LoginSignUpPresenter(bus, emailUseCase);
    }

    @Provides
    GoalCategoriesPresenter providesGoalCategoriesPresenter(EventBus bus, GetCategoriesUseCase useCase) {
        return new GoalCategoriesPresenter(bus, useCase);
    }

    @Provides
    GoalWeightPresenter providesGoalWeightPresenter(EventBus bus, NewGoalUseCase useCase, GoalMapper mapper) {
        return new GoalWeightPresenter(bus, useCase, mapper);
    }

    @Provides
    FeedPresenter providesFeedPresenter(EventBus bus, GetFeedUseCase useCase) {
        return new FeedPresenter(bus, useCase);
    }

    @Provides
    InterestPresenter providesGoalAnswerTypePresenter(EventBus bus, GetCategoriesUseCase categoriesUseCase, AddInterestUseCase addInterestUseCase, RemoveInterestUseCase removeInterestUseCase, InterestMapper mapper) {
        return new InterestPresenter(bus, categoriesUseCase, addInterestUseCase, removeInterestUseCase, mapper);
    }

    @Provides
    QuestionDetailPresenter providesQuestionDetailPresenter(EventBus bus) {
        return new QuestionDetailPresenter(bus);
    }
}
