package com.doisdoissete.moovup.di.components;

/**
 * Created by broto on 8/28/15.
 */
public interface HasComponent<C> {
    C getComponent();
}