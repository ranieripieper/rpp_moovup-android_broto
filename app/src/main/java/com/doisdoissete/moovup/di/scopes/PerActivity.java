package com.doisdoissete.moovup.di.scopes;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by broto on 8/28/15.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {}