package com.doisdoissete.moovup.di.components;


import com.doisdoissete.moovup.di.modules.AndroidModule;
import com.doisdoissete.moovup.di.modules.PresenterModule;
import com.doisdoissete.moovup.ui.activities.BaseActivity;
import com.doisdoissete.moovup.ui.activities.GoalActivity;
import com.doisdoissete.moovup.ui.activities.LoginActivity;
import com.doisdoissete.moovup.ui.activities.LoginGoogleActivity;
import com.doisdoissete.moovup.ui.activities.MainActivity;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;
import com.doisdoissete.moovup.ui.fragments.goals.InterestFragment;
import com.doisdoissete.moovup.ui.fragments.goals.GoalCategoriesFragment;
import com.doisdoissete.moovup.ui.fragments.goals.GoalWeightFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginFormFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginFormSignUpFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginFormSocialNetworksFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginIntroFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginSignInFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginSignUpFragment;
import com.doisdoissete.moovup.ui.fragments.login.SignUpCompanyFragment;
import com.doisdoissete.moovup.ui.fragments.login.SignUpPersonFragment;
import com.doisdoissete.moovup.ui.fragments.questions.FeedFragment;
import com.doisdoissete.moovup.ui.fragments.questions.QuestionDetailFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                AndroidModule.class,
                PresenterModule.class
        }
)
public interface ApplicationComponent {

    /***************************/
    /** Activities injections **/
    /***************************/

    void inject(BaseActivity activity);
    void inject(MainActivity activity);
    void inject(GoalActivity activity);
    void inject(LoginActivity activity);
    void inject(LoginGoogleActivity activity);

    /**************************/
    /** Fragments injections **/
    /**************************/

    void inject(BaseFragment fragment);
    void inject(LoginFormFragment fragment);
    void inject(LoginFormSignUpFragment fragment);
    void inject(LoginFormSocialNetworksFragment fragment);
    void inject(LoginIntroFragment fragment);
    void inject(LoginSignInFragment fragment);
    void inject(LoginSignUpFragment fragment);

    void inject(SignUpCompanyFragment fragment);
    void inject(SignUpPersonFragment fragment);

    void inject(GoalCategoriesFragment fragment);
    void inject(GoalWeightFragment fragment);
    void inject(InterestFragment fragment);

    void inject(FeedFragment fragment);
    void inject(QuestionDetailFragment fragment);

    /*************************/
    /** Services injections **/
    /*************************/
}
