package com.doisdoissete.moovup.di.modules;

import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import com.doisdoissete.data.net.AuthenticatedApi;
import com.doisdoissete.data.net.PublicApi;
import com.doisdoissete.data.net.RestApi;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.di.scopes.ForApplication;
import com.doisdoissete.moovup.navigation.Navigator;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;
import io.realm.Realm;


/**
 * A module for Android-specific dependencies which require a {@link Context} or
 * {@link android.app.Application} to create.
 */
@Module
public class AndroidModule {

    private final MoovUpApplication application;

    public AndroidModule(MoovUpApplication application) {
        this.application = application;
    }
    /**
     * Allow the application context to be injected but require that it be annotated with
     * {@link ForApplication @Annotation} to explicitly differentiate it from an activity context.
     */
    @Provides
    @Singleton
    @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }

    @Provides
    NotificationManager provideNotificationManager() {
        return getSystemService(application, Context.NOTIFICATION_SERVICE);
    }

    @Provides
    ConnectivityManager provideConnectivityManager() {
        return getSystemService(application, Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    SharedPreferences provideSharedPrefs() {
        return application.getSharedPreferences("MoovUp_prefs", Context.MODE_PRIVATE);
    }

    @Provides
    Navigator provideNavigator() {
        return new Navigator();
    }

    @Provides
    EventBus provideBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Named("PublicAPI")
    RestApi providesRespApi(PublicApi restApi) {
        return restApi;
    }

    @Provides
    @Named("AuthenticatedAPI")
    RestApi providesRespApiLogged(AuthenticatedApi restApi) {
        return restApi;
    }

    @SuppressWarnings("unchecked")
    private <T> T getSystemService(Context context, String serviceConstant) {
        return (T) context.getSystemService(serviceConstant);
    }


}