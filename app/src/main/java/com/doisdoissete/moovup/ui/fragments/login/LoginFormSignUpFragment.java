package com.doisdoissete.moovup.ui.fragments.login;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.LoginSignUpPresenter;
import com.doisdoissete.moovup.ui.custom.validation.AtLeastOneLetter;
import com.doisdoissete.moovup.ui.custom.validation.AtLeastOneNumber;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.LoginSignUpView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by broto on 9/12/15.
 */
public class LoginFormSignUpFragment extends BaseView<LoginSignUpPresenter> implements Validator.ValidationListener, LoginSignUpView {

    @NotEmpty(messageResId = R.string.required_field)
    @Email(messageResId = R.string.invalid_email)
    @Bind(R.id.txt_signup_email)
    public AppCompatAutoCompleteTextView emailEditText;

    @AtLeastOneLetter
    @AtLeastOneNumber
    @Length(min = 6, messageResId = R.string.password_min_char_count)
    @Password(messageResId = R.string.invalid_password)
    @Bind(R.id.txt_signup_password)
    public EditText passwordEditText;

    @ConfirmPassword(messageResId = R.string.passwords_dont_match)
    @Bind(R.id.txt_signup_confirm_password)
    public EditText confirmPasswordEditText;

    @Bind(R.id.progress_signup_email)
    public ProgressBar progressBar;

    @Bind(R.id.btn_signup)
    public Button btnSignUp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initEmailField();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_form_signup;
    }

    @OnClick(R.id.btn_signup)
    public void onSignUpClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(this);
        validator.validate();
    }

    @OnClick(R.id.lbl_signup_terms)
    public void onTermsClick() {
        navigator.navigateToTermsAndConditionsFragment(getActivity());
    }

    @Override
    public void onValidationSucceeded() {
        navigator.navigateToProfileFragment(getActivity(),
                emailEditText.getText().toString(),
                passwordEditText.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }

    Timer timer = new Timer();
    final long DELAY = 1000;
    boolean isFirstInput = true;

    private void initEmailField() {
        // AutoComplete Edittext with device emails
        List<String> emails = getSuggestedEmails();
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, emails);
        emailEditText.setAdapter(adapter);

        // Text Watcher
        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null)
                    timer.cancel();
            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (isFirstInput && s.length() > 5) {
                    isFirstInput = false;
                    return;
                }

                isFirstInput = false;
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (presenter.isValidEmail(s.toString())) {
                            // Show progress indicator (MainThread)
                            progressBar.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.VISIBLE);
                                    presenter.isEmailRegistered(s.toString());
                                }
                            });
                        } else {
                            // Hide progress indicator (MainThread)
                            progressBar.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                    btnSignUp.setEnabled(false);
                                }
                            });
                        }
                    }

                }, DELAY);
            }
        });
    }

    private List<String> getSuggestedEmails() {
        List<String> emails = new ArrayList<>();
        Account[] accounts = AccountManager.get(getActivity()).getAccounts();

        for (Account account : accounts) {
            if (presenter.isValidEmail(account.name) && !emails.contains(account.name))
                emails.add(account.name);
        }

        return emails;
    }

    @Override
    public void showEmailLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmailLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setInvalidForm() {
        btnSignUp.setEnabled(false);
        emailEditText.setError(getString(R.string.email_already_taken));
    }

    @Override
    public void setValidForm() {
        btnSignUp.setEnabled(true);
    }
}
