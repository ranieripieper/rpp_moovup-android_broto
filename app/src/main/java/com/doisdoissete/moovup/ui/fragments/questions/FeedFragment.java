package com.doisdoissete.moovup.ui.fragments.questions;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.domain.entity.Question;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.FeedPresenter;
import com.doisdoissete.moovup.ui.adapters.QuestionViewHolder;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.FeedView;

import org.solovyev.android.views.llm.DividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import hugo.weaving.DebugLog;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by broto on 10/27/15.
 */
public class FeedFragment extends BaseView<FeedPresenter> implements FeedView, QuestionViewHolder.QuestionHolderListener {

    @Bind(R.id.swipe_refresh_layout_feed)
    public SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.recycler_feed)
    public RecyclerView mRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initSwipeRefresh();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_feed, menu);

        Drawable drawable = menu.findItem(R.id.menu_feed_search).getIcon();
        drawable = DrawableCompat.wrap(drawable);

        DrawableCompat.setTint(drawable, getResources().getColor(R.color.white));
        menu.findItem(R.id.menu_feed_search).setIcon(drawable);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void initSwipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onRefresh();
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_feed;
    }

    public static FeedFragment newInstance() {
        return new FeedFragment();
    }

    @DebugLog
    @Override
    public void renderQuestion(List<Question> questionList) {
        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null));
        mRecyclerView.setAdapter(new EasyRecyclerAdapter<>(
                getActivity(),
                QuestionViewHolder.class,
                questionList,
                this));
    }

    @Override
    public void showErrorMessage() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.invalid)
                .content(getString(R.string.error_loading_feed))
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .icon(getResources().getDrawable(R.drawable.icn_error_alert))
                .show();
    }

    @Override
    public void hideLoadingView() {
        super.hideLoadingView();

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onQuestionSelected(Question question) {
        navigator.navigateToQuestionDetailsFragment(getActivity(), question);
    }
}
