package com.doisdoissete.moovup.ui.fragments.login;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.ui.activities.BaseActivity;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;

import butterknife.OnClick;

/**
 * Created by broto on 9/16/15.
 */
public class LoginIntroFragment extends BaseFragment {

    public static LoginIntroFragment newInstance() {
        return new LoginIntroFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    public void onResumeFromBackstack() {
        super.onResumeFromBackstack();

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_login_intro;
    }

    @OnClick(R.id.btn_login_enter)
    public void onEnterClick() {
        navigator.navigateToSignInFragment(getActivity());
    }

    @OnClick(R.id.lbl_login_wanna_signup)
    public void onSignUpClick() {
        navigator.navigateToSignUpFragment(getActivity());
    }

    // TODO: Find a better way to avoid fragment recreation
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            Fragment socialForm = getFragmentManager().findFragmentById(R.id.fragment_login_intro_social_networks);

            if (socialForm != null)
                transaction.remove(socialForm);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
