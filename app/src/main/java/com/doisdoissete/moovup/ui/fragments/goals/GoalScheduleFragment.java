package com.doisdoissete.moovup.ui.fragments.goals;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.ui.activities.BaseActivity;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by broto on 10/16/15.
 */
public class GoalScheduleFragment extends BaseFragment implements Validator.ValidationListener {

    private static final String ARG_CATEGORY = "arg_category";

    @Bind(R.id.lbl_goal_schedule_title)
    public TextView lblTitle;

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.txt_goal_schedule_amount)
    public EditText txtAmount;

    private String periodicityType = "day";
    private Category category;

    public static GoalScheduleFragment newInstance(Category category) {
        GoalScheduleFragment fragment = new GoalScheduleFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_CATEGORY, category);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            category = getArguments().getParcelable(ARG_CATEGORY);
        }

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    public void onResumeFromBackstack() {
        super.onResumeFromBackstack();

        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lblTitle.setText(getString(R.string.how_many_minutes_of) + " " + category.getTitle() + getString(R.string.do_wanna_practice));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_schedule;
    }

    @OnClick(R.id.btn_goal_schedule_submit)
    public void onSubmitClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(this);
        validator.validate();
    }

    @OnClick(R.id.radio_day)
    public void onRadioDayClick() {
        periodicityType = "day";
    }

    @OnClick(R.id.radio_week)
    public void onRadioWeekClick() {
        periodicityType = "week";
    }

    @OnClick(R.id.radio_month)
    public void onRadioMonthClick() {
        periodicityType = "month";
    }

    @Override
    public void onValidationSucceeded() {
        navigator.navigateToGoalWeightFragment(getActivity(), Integer.valueOf(txtAmount.getText().toString()), periodicityType, category);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }
}
