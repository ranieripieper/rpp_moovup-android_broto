package com.doisdoissete.moovup.ui.fragments.login;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;

/**
 * Created by broto on 9/16/15.
 */
public class LoginSignUpFragment extends BaseFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_login_signup;
    }

    // TODO: Find a better way to avoid fragment recreation
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            Fragment signUpForm =  getFragmentManager().findFragmentById(R.id.fragment_signup_form);
            Fragment socialForm =  getFragmentManager().findFragmentById(R.id.fragment_signup_social_networks);

            if (signUpForm != null)
                transaction.remove(signUpForm);

            if (socialForm != null)
                transaction.remove(socialForm);

            transaction.commit();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
