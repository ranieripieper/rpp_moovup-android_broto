package com.doisdoissete.moovup.ui.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.view.Menu;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.di.components.HasComponent;
import com.doisdoissete.moovup.ui.fragments.goals.InterestFragment;
import com.doisdoissete.moovup.ui.fragments.goals.GoalCategoriesFragment;

import butterknife.Bind;

/**
 * Created by broto on 9/22/15.
 */
public class GoalActivity extends BaseActivity implements HasComponent<ApplicationComponent> {

    public static final String ARG_NEW_USER = "arg_new_user";

    @Bind(R.id.appbar)
    public AppBarLayout appBarLayout;

    private boolean newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        newUser = getIntent().getBooleanExtra(ARG_NEW_USER, false);

        this.getApplicationComponent().inject(this);

        if (getToolbar() != null)
            getToolbar().setTitle(R.string.add_new_goals);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_goal;
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_main_container;
    }

    @Override
    protected Fragment getMainFragment() {
        return GoalCategoriesFragment.newInstance();
    }

    @Override
    public ApplicationComponent getComponent() {
        return getApplicationComponent();
    }

    @Override
    public void addFragment(Fragment fragment) {
        if (fragment instanceof InterestFragment) {
            if (!newUser) {
                finish();
            }
        }

        super.addFragment(fragment);

        if (getFragmentManager().getBackStackEntryCount() > 0) {
            appBarLayout.setExpanded(false, true);
        } else {
            appBarLayout.setExpanded(true, true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
