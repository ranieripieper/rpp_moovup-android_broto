package com.doisdoissete.moovup.ui.views;

/**
 * Created by broto on 9/18/15.
 */
public interface LoginView extends LoadingView{

    void emptyFields();
    void showErrorDialog(String message);
    void showMainActivity();
    void finishActivity();

    void showSuccessDialog(String message);
}
