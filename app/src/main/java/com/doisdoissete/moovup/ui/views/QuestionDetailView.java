package com.doisdoissete.moovup.ui.views;

import com.doisdoissete.domain.entity.Question;

/**
 * Created by broto on 11/6/15.
 */
public interface QuestionDetailView {
    void renderQuestion(Question question);
}
