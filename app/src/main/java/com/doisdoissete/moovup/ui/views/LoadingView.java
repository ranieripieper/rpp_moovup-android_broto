package com.doisdoissete.moovup.ui.views;

/**
 * Created by broto on 9/23/15.
 */
public interface LoadingView {

    void showLoadingView();
    void hideLoadingView();
}
