package com.doisdoissete.moovup.ui.fragments.questions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;

/**
 * Created by broto on 11/6/15.
 */
public class MyQuestionsFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (view != null)
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });

        return view;
    }

    public static MyQuestionsFragment newInstance() {

        Bundle args = new Bundle();

        MyQuestionsFragment fragment = new MyQuestionsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_questions;
    }
}
