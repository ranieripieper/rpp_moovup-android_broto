package com.doisdoissete.moovup.ui.fragments.login;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;

import butterknife.Bind;

/**
 * Created by broto on 9/24/15.
 */
public class TermsFragment extends BaseFragment {

    @Bind(R.id.webview_terms_conditions)
    public WebView webView;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_terms_conditions;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        webView.loadUrl("file:///android_asset/terms.html");
    }
}
