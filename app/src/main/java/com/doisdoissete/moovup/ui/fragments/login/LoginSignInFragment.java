package com.doisdoissete.moovup.ui.fragments.login;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;

/**
 * Created by broto on 9/16/15.
 */
public class LoginSignInFragment extends BaseFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_login_signin;
    }

    // TODO: Find a better way to avoid fragment recreation
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            Fragment signInForm =  getFragmentManager().findFragmentById(R.id.fragment_signin_loginform);
            Fragment socialForm =  getFragmentManager().findFragmentById(R.id.fragment_signin_social_networks);

            if (signInForm != null)
                transaction.remove(signInForm);

            if (socialForm != null)
                transaction.remove(socialForm);

            transaction.commit();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
