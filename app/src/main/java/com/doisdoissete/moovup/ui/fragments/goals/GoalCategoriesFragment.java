package com.doisdoissete.moovup.ui.fragments.goals;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.GoalCategoriesPresenter;
import com.doisdoissete.moovup.ui.adapters.CategoryGroupViewHolder;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.GoalCategoriesView;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class GoalCategoriesFragment extends BaseView<GoalCategoriesPresenter> implements GoalCategoriesView {

    @Bind(R.id.layout_goal_categories_recycler_view)
    public RecyclerView mRecyclerView;

    public static GoalCategoriesFragment newInstance() {
        return new GoalCategoriesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_categories;
    }

    @Override
    public void renderGoals(List<Category> categoryList) {
        LinearLayoutManager layoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(new EasyRecyclerAdapter<>(
                getActivity(),
                CategoryGroupViewHolder.class,
                categoryList));
    }

    @OnClick(R.id.btn_goal_categories_submit)
    public void onProccedClick() {
        navigator.navigateToGoalAnswerTypeFragment(getActivity());
    }
}
