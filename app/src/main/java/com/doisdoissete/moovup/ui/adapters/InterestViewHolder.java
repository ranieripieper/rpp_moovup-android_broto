package com.doisdoissete.moovup.ui.adapters;

import android.content.DialogInterface;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.domain.entity.Interest;
import com.doisdoissete.moovup.R;
import com.serchinastico.coolswitch.CoolSwitch;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 11/3/15.
 */
@LayoutId(R.layout.row_interest)
public class InterestViewHolder extends ItemViewHolder<Category> {

    @ViewId(R.id.lbl_interest_title)
    TextView lblTitle;

    @ViewId(R.id.lbl_interest_subtitle)
    TextView lblSubtitle;

    @ViewId(R.id.chk_interest)
    SwitchCompat chkEnabled;

    public InterestViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Category item, PositionInfo positionInfo) {
        lblTitle.setText(item.getTitle());
        chkEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    showFrequencyDialog();
                } else {
                    lblSubtitle.setText("");

                    InterestHolderListener listener = getListener(InterestHolderListener.class);
                    if (listener != null) {
                        Interest interest = new Interest();
                        interest.setActive(false);
                        interest.setCategoryId(getItem().getId());
                        listener.onInterestChanged(interest);
                    }
                }
            }
        });
    }

    private void showFrequencyDialog() {
        final Interest interest = new Interest();
        interest.setActive(true);

        new MaterialDialog.Builder(getContext())
                .title(String.format(getContext().getString(R.string.how_many_questions_you_wanna_answer_about), getItem().getTitle()))
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .items(R.array.questions_amount)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                interest.setQuestionCount(1);
                                break;
                            case 1:
                                interest.setQuestionCount(5);
                                break;
                            case 2:
                                interest.setQuestionCount(10);
                                break;
                            case 3:
                                interest.setQuestionCount(20);
                                break;
                            case 4:
                                interest.setQuestionCount(10000);
                                break;
                            case 5:
                                interest.setQuestionCount(0);
                                break;
                            default:
                                break;
                        }
                        return true;
                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        lblSubtitle.setText(String.format(getContext().getString(R.string.answer_up_to), interest.getQuestionCount()));

                        InterestHolderListener listener = getListener(InterestHolderListener.class);
                        if (listener != null) {
                            interest.setCategoryId(getItem().getId());
                            listener.onInterestChanged(interest);
                        }
                    }
                })
                .show();
    }

    @Override
    public void onSetListeners() {
    }

    public interface InterestHolderListener {
        void onInterestChanged(Interest interest);
    }
}