package com.doisdoissete.moovup.ui.views;

/**
 * Created by broto on 9/18/15.
 */
public interface LoginSignUpView {

    void showEmailLoading();
    void hideEmailLoading();
    void setInvalidForm();
    void setValidForm();
}
