package com.doisdoissete.moovup.ui.views;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Created by broto on 9/18/15.
 */
public interface LoginSocialNetWorksView extends LoadingView{

    void showErrorDialog(String message);
    void showErrorDialog(@StringRes int messageRes);
    void showMainActivity();
    void showGoalActivity();
    void showGooglePlusActivity();
    void getFacebookToken();
    void finishActivity();

    Context getContext();
}
