package com.doisdoissete.moovup.ui.views;

import com.doisdoissete.domain.entity.Career;
import com.doisdoissete.domain.entity.Hobby;

import java.util.List;

/**
 * Created by broto on 9/22/15.
 */
public interface SignUpPersonView extends LoadingView{

    void showErrorDialog(String message);
    void showGoalActivity();
    void renderCareers(List<Career> careers);
    void renderHobbies(List<Hobby> hobbies);
    void showMediaTypeChooser();
}
