package com.doisdoissete.moovup.ui.fragments.login;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.LoginEmailPresenter;
import com.doisdoissete.moovup.ui.custom.validation.AtLeastOneLetter;
import com.doisdoissete.moovup.ui.custom.validation.AtLeastOneNumber;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.LoginView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by broto on 9/16/15.
 */
public class LoginFormFragment extends BaseView<LoginEmailPresenter> implements Validator.ValidationListener, LoginView {

    @NotEmpty(messageResId = R.string.required_field)
    @Email(messageResId = R.string.invalid_email)
    @Bind(R.id.txt_login_email)
    public EditText emailEditText;

    @AtLeastOneLetter
    @AtLeastOneNumber
    @Length(min = 6, messageResId = R.string.password_min_char_count)
    @Password(messageResId = R.string.invalid_password)
    @Bind(R.id.txt_login_password)
    public EditText passwordEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @OnClick(R.id.lbl_login_forgot_password)
    public void onForgotPasswordClick() {
        if (emailEditText.getText().toString().isEmpty()) {
            emailEditText.setError("Email not valid");
        } else {
            presenter.forgotPasswordClicked(emailEditText.getText().toString());
        }
    }

    @OnClick(R.id.btn_signup)
    public void onEnterClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(this);
        validator.validate();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_form_login;
    }

    @Override
    public void onValidationSucceeded() {
        presenter.signIn(emailEditText.getText().toString(), passwordEditText.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }

    @Override
    public void emptyFields() {
        emailEditText.setText("");
        passwordEditText.setText("");
    }

    @Override
    public void showSuccessDialog(String message) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.success)
                .content(message)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .icon(getResources().getDrawable(R.drawable.icn_success))
                .show();
    }

    @Override
    public void showErrorDialog(String message) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.invalid)
                .content(message)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .icon(getResources().getDrawable(R.drawable.icn_error_alert))
                .show();
    }

    @Override
    public void showMainActivity() {
        navigator.navigateToMainActivity(getActivity());
    }

    @Override
    public void finishActivity() {
        getActivity().finish();
    }
}
