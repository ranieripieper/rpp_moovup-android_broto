package com.doisdoissete.moovup.ui.fragments.goals;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.domain.entity.Interest;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.InterestPresenter;
import com.doisdoissete.moovup.ui.adapters.CategoryGroupViewHolder;
import com.doisdoissete.moovup.ui.adapters.InterestViewHolder;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.InterestView;

import org.joda.time.DateTime;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by broto on 10/30/15.
 */
public class InterestFragment extends BaseView<InterestPresenter> implements InterestView, InterestViewHolder.InterestHolderListener {

    @Bind(R.id.recycler_interest)
    public RecyclerView mRecyclerView;

    public static InterestFragment newInstance() {
        Bundle args = new Bundle();

        InterestFragment fragment = new InterestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResumeFromBackstack() {
        super.onResumeFromBackstack();

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_answer_type;
    }

    @Override
    public void renderCategories(List<Category> categoryList) {
        LinearLayoutManager layoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(new EasyRecyclerAdapter<>(
                getActivity(),
                InterestViewHolder.class,
                categoryList,
                this));
    }

    @Override
    public void onInterestChanged(Interest interest) {
        presenter.handleInterest(interest);
    }

    @OnClick(R.id.btn_goal_schedule_submit)
    public void onSubmitClick() {
        navigator.navigateToFeedFragment(getActivity());
    }

}
