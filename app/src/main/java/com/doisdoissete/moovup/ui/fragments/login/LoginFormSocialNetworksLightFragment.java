package com.doisdoissete.moovup.ui.fragments.login;

import com.doisdoissete.moovup.R;

/**
 * Created by broto on 9/14/15.
 */
public class LoginFormSocialNetworksLightFragment extends LoginFormSocialNetworksFragment {

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_form_social_networks_light;
    }
}
