package com.doisdoissete.moovup.ui.views;

import com.doisdoissete.domain.entity.Category;

import java.util.List;

/**
 * Created by broto on 10/13/15.
 */
public interface GoalCategoriesView extends LoadingView{

    void renderGoals(List<Category> categoryList);
}
