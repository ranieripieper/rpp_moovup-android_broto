package com.doisdoissete.moovup.ui.views;

/**
 * Created by broto on 10/16/15.
 */
public interface GoalWeightView extends LoadingView {
    void showSuccessMessage();

    void showErrorMessage();

}
