package com.doisdoissete.moovup.ui.views;

import com.doisdoissete.domain.entity.Question;

import java.util.List;

/**
 * Created by broto on 10/27/15.
 */
public interface FeedView extends LoadingView{

    void renderQuestion(List<Question> questionList);

    void showErrorMessage();
}
