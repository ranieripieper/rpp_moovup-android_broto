package com.doisdoissete.moovup.ui.views;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.presenters.BasePresenter;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;

import javax.inject.Inject;

/**
 * Created by broto on 6/29/15.
 */
public abstract class BaseView<P extends BasePresenter> extends BaseFragment {

    @Inject
    protected P presenter;

    private ProgressDialog progress;

    public void showLoadingView() {
        if (progress == null || !progress.isShowing()) {
            progress = ProgressDialog.show(getActivity(), getString(R.string.wait),
                    getString(R.string.loading), true);
            hideKeyboard();
        }
    }

    public void hideLoadingView() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.attachView(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        presenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();

        presenter.pause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.destroy();
    }
}
