package com.doisdoissete.moovup.ui.fragments.login;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.ui.adapters.SignUpViewPagerAdapter;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;

import butterknife.Bind;

/**
 * Created by broto on 9/29/15.
 */
public class ProfileFragment extends BaseFragment {

    private static final String ARG_EMAIL = "param_email";
    private static final String ARG_PASSWORD = "param_password";

    @Bind(R.id.viewpager_profile)
    public ViewPager viewPager;

    @Bind(R.id.tablayout_profile)
    public TabLayout tabLayout;

    private String email;
    private String password;

    public static ProfileFragment newInstance(String email, String password) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_EMAIL, email);
        bundle.putString(ARG_PASSWORD, password);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(ARG_EMAIL);
            password = getArguments().getString(ARG_PASSWORD);
        }

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_profile_pager;
    }

    private void setupViewPager(ViewPager viewPager) {
        SignUpViewPagerAdapter adapter = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            adapter = new SignUpViewPagerAdapter(getChildFragmentManager());
        } else {
            adapter = new SignUpViewPagerAdapter(getFragmentManager());
        }

        adapter.addFrag(SignUpPersonFragment.newInstance(email,password), getString(R.string.person));
        adapter.addFrag(SignUpCompanyFragment.newInstance(email,password), getString(R.string.company));
        viewPager.setAdapter(adapter);
    }
}
