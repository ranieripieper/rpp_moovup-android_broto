package com.doisdoissete.moovup.ui.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.di.components.HasComponent;
import com.doisdoissete.moovup.ui.fragments.questions.FeedFragment;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by broto on 10/27/15.
 */
public class MainActivity extends BaseActivity implements HasComponent<ApplicationComponent> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getApplicationComponent().inject(this);

        initNavigationView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_fragment_container;
    }

    @Override
    protected Fragment getMainFragment() {
        return FeedFragment.newInstance();
//        return InterestFragment.newInstance();
    }

    @Override
    public ApplicationComponent getComponent() {
        return getApplicationComponent();
    }

    private void initNavigationView() {
        if (mNavigationView != null) {
            initNavigationHeader();

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    if (item.isChecked()) {
                        item.setChecked(false);
                    } else {
                        item.setChecked(true);

                        if (mDrawerLayout != null)
                            mDrawerLayout.closeDrawers();

                        switch (item.getItemId()) {
                            case R.id.drawer_home:
                                navigator.navigateToFeedFragment(MainActivity.this);
                                break;
                            case R.id.drawer_my_questions:
                                navigator.navigateToMyQuestionsFragment(MainActivity.this);
                                break;
                            case R.id.drawer_to_answer:
                                navigator.navigateToMyQuestionsFragment(MainActivity.this);
                                break;
                            case R.id.drawer_favorited_questions:
                                navigator.navigateToMyQuestionsFragment(MainActivity.this);
                                break;
                            case R.id.drawer_my_friends:
                                navigator.navigateToContactsFragment(MainActivity.this);
                                break;
                            case R.id.drawer_my_goals:
                                navigator.navigateToGoalsFragment(MainActivity.this);
                                break;
                            case R.id.drawer_profile:
                                navigator.navigateToProfileActivity(MainActivity.this);
                                break;
                            case R.id.drawer_notifications:
                                navigator.navigateToNotificationsFragment(MainActivity.this);
                                break;
                            case R.id.drawer_help:
                                navigator.navigateToHelpFragment(MainActivity.this);
                                break;
                            default:
                                break;
                        }
                    }

                    return true;
                }
            });
        }
    }

    private void initNavigationHeader() {
        View headerView = mNavigationView.inflateHeaderView(R.layout.drawer_header);
        TextView lblNavigationHeaderName = (TextView) headerView.findViewById(R.id.lbl_drawer_name);
        CircleImageView imgPhoto = (CircleImageView) headerView.findViewById(R.id.img_drawer_photo);
        ImageView imgSettings = (ImageView) headerView.findViewById(R.id.img_drawer_settings);

        imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigator.navigateToProfileActivity(MainActivity.this);
            }
        });

        imgSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigator.navigateToSettingsFragment(MainActivity.this);
            }
        });
    }
}