package com.doisdoissete.moovup.ui.fragments.goals;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.domain.entity.Goal;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.GoalWeightPresenter;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.GoalWeightView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by broto on 10/16/15.
 */
public class GoalWeightFragment extends BaseView<GoalWeightPresenter> implements GoalWeightView, Validator.ValidationListener {

    private static final String ARG_GOAL = "arg_goal";

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.txt_goal_weight_current)
    public EditText txtCurrentWeight;

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.txt_goal_weight_goal)
    public EditText txtTargetWeight;

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.txt_goal_weight_date)
    public EditText txtDate;

    @Bind(R.id.lbl_share_with_friends)
    public CheckedTextView lblShareGoalWithFriends;

    private DatePickerDialog datePicker;

    private Goal goal;

    public static GoalWeightFragment newInstance(Goal goal) {
        GoalWeightFragment fragment = new GoalWeightFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_GOAL, goal);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            goal = getArguments().getParcelable(ARG_GOAL);
        }

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDateField();
        initShareField();
    }

    private void initShareField() {
        lblShareGoalWithFriends.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckedTextView) v).isChecked()) {
                    ((CheckedTextView) v).setChecked(false);
                } else {
                    ((CheckedTextView) v).setChecked(true);
                }
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_weight;
    }

    @OnClick(R.id.btn_goal_weight_submit)
    public void onSubmitClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(this);
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        goal.setCurrentWeight(Float.valueOf(txtCurrentWeight.getText().toString()));
        goal.setTargetWeight(Float.valueOf(txtTargetWeight.getText().toString()));
        goal.setTargetDate(txtDate.getText().toString());
        goal.setmPublic(lblShareGoalWithFriends.isChecked());

        presenter.saveGoal(goal);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }

    private void initDateField() {
        txtDate.setOnKeyListener(null);
        txtDate.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();

                if (MotionEvent.ACTION_UP == event.getAction()) {
                    buildDatePicker().show();
                }

                return true;
            }
        });
    }

    private DatePickerDialog buildDatePicker() {
        if (datePicker == null) {
            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            datePicker = new DatePickerDialog(getActivity(),
                    R.style.DialogTheme,
                    datePickerListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH) + 1);
            datePicker.setCancelable(false);
        }

        return datePicker;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            String year1 = String.valueOf(selectedYear);
            String month1 = String.valueOf(selectedMonth + 1);
            String day1 = String.valueOf(selectedDay);
            txtDate.setText(day1 + "/" + month1 + "/" + year1);
        }
    };

    @Override
    public void showSuccessMessage() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.success)
                .content(getString(R.string.goal_added_success))
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        navigator.navigateToGoalCategoriesFragment(getActivity());
                    }
                })
                .icon(getResources().getDrawable(R.drawable.icn_success))
                .show();
    }

    @Override
    public void showErrorMessage() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.success)
                .content(getString(R.string.goal_add_error))
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .icon(getResources().getDrawable(R.drawable.icn_error_alert))
                .show();
    }
}
