package com.doisdoissete.moovup.ui.custom.validation;

import com.doisdoissete.moovup.R;
import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by broto on 10/12/15.
 */

@ValidateUsing(AtLeastOneLetterRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface AtLeastOneLetter {

    int min()           default 6;
    int sequence()      default -1;
    int messageResId()  default R.string.password_at_least_one_letter;
    String message()    default "";
}
