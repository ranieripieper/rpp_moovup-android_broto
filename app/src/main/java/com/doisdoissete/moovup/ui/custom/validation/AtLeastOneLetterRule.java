package com.doisdoissete.moovup.ui.custom.validation;

import com.mobsandgeeks.saripaar.AnnotationRule;

/**
 * Created by broto on 10/13/15.
 */
public class AtLeastOneLetterRule extends AnnotationRule<AtLeastOneLetter, String> {

    protected AtLeastOneLetterRule(AtLeastOneLetter atLeastOneLetter) {
        super(atLeastOneLetter);
    }

    @Override
    public boolean isValid(String data) {
        boolean hasLetter = data.matches(".*[a-zA-Z].*");

        return hasLetter;
    }
}