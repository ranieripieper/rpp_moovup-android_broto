package com.doisdoissete.moovup.ui.fragments.questions;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doisdoissete.domain.entity.Question;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.QuestionDetailPresenter;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.QuestionDetailView;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by broto on 11/6/15.
 */
public class QuestionDetailFragment extends BaseView<QuestionDetailPresenter> implements QuestionDetailView {

    private static final String ARG_QUESTION = "arg_question";
    private Question question;

    @Bind(R.id.img_feed_photo)
    public CircleImageView imgPhoto;

    @Bind(R.id.lbl_question_name)
    public TextView lblName;

    @Bind(R.id.lbl_question_date)
    public TextView lblDate;

    @Bind(R.id.lbl_question_category)
    public TextView lblCategory;

    @Bind(R.id.lbl_question_content)
    public TextView lblContent;

    @Bind(R.id.lbl_question_answer_count)
    public TextView lblAnswer;

    public static QuestionDetailFragment newInstance(Question question) {

        Bundle args = new Bundle();
        args.putParcelable(ARG_QUESTION, question);

        QuestionDetailFragment fragment = new QuestionDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);

        setHasOptionsMenu(true);

        if (getArguments() != null) {
            question = getArguments().getParcelable(ARG_QUESTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (view != null)
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.setQuestion(question);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_question_details, menu);

        Drawable drawableFavorite = menu.findItem(R.id.menu_question_favorite).getIcon();
        Drawable drawableShare = menu.findItem(R.id.menu_question_share).getIcon();

        drawableFavorite = DrawableCompat.wrap(drawableFavorite);
        drawableShare = DrawableCompat.wrap(drawableShare);

        DrawableCompat.setTint(drawableFavorite, getResources().getColor(R.color.white));
        DrawableCompat.setTint(drawableShare, getResources().getColor(R.color.white));

        menu.findItem(R.id.menu_question_favorite).setIcon(drawableFavorite);
        menu.findItem(R.id.menu_question_share).setIcon(drawableShare);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_question_details;
    }

    @Override
    public void renderQuestion(Question question) {
        if (question.getUser().getProfileImage() != null && question.getUser().getProfileImage().getThumb() != null)
            Picasso.with(getActivity()).load(question.getUser().getProfileImage().getThumb()).placeholder(R.drawable.user_placeholder).into(imgPhoto);

        lblName.setText(question.getUser().getFullName());
        lblCategory.setText(question.getCategory().getTitle());
        lblContent.setText(question.getBodyText());

        DateFormat df = new DateFormat();
        lblDate.setText(df.format("dd/MM/yyyy", question.getCreatedAt()));
    }
}
