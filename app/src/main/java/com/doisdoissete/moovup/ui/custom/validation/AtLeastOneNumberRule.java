package com.doisdoissete.moovup.ui.custom.validation;

import com.mobsandgeeks.saripaar.AnnotationRule;

/**
 * Created by broto on 10/13/15.
 */
public class AtLeastOneNumberRule extends AnnotationRule<AtLeastOneNumber, String> {

    protected AtLeastOneNumberRule(AtLeastOneNumber atLeastOneNumber) {
        super(atLeastOneNumber);
    }

    @Override
    public boolean isValid(String data) {
        boolean hasNumber = data.matches(".*\\d+.*");

        return hasNumber;
    }
}