package com.doisdoissete.moovup.ui.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.di.components.HasComponent;
import com.doisdoissete.moovup.navigation.Navigator;
import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by broto on 8/26/15.
 */
public abstract class BaseFragment extends Fragment {

    @Inject
    public Navigator navigator;

    protected abstract int getLayoutResource();

    /**
     * Closes keyboard on fragment first appearance.
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        hideKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    public void onResumeFromBackstack() {
        hideKeyboard();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * Used by Leak Canary
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = MoovUpApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    /**
     * Dagger Injection of annotated fields.
     *
     * @param componentType
     * @param <C>
     * @return
     */
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    public void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
}
