package com.doisdoissete.moovup.ui.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import com.doisdoissete.domain.interactor.GetLoggedUserUseCase;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.di.components.HasComponent;
import com.doisdoissete.moovup.ui.fragments.login.LoginIntroFragment;
import com.sromku.simple.fb.SimpleFacebook;

import javax.inject.Inject;

/**
 * Created by broto on 9/11/15.
 */
public class LoginActivity extends BaseActivity implements HasComponent<ApplicationComponent> {

    @Inject
    public GetLoggedUserUseCase useCase;

    private SimpleFacebook mSimpleFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getApplicationComponent().inject(this);

        if (useCase.getuser() != null) {
            navigator.navigateToMainActivity(this);
            finish();
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_login_container;
    }

    @Override
    protected Fragment getMainFragment() {
        return LoginIntroFragment.newInstance();
    }

    @Override
    public ApplicationComponent getComponent() {
        return getApplicationComponent();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
    }
}
