package com.doisdoissete.moovup.ui.adapters;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.moovup.R;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_category)
public class CategoryViewHolder extends ItemViewHolder<Category> {

    @ViewId(R.id.card_view)
    CardView card;

    @ViewId(R.id.lbl_row_category)
    TextView lblTitle;

    public CategoryViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Category item, PositionInfo positionInfo) {
        lblTitle.setText(item.getTitle());
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoryHolderListener listener = getListener(CategoryHolderListener.class);
                if (listener != null) {
                    listener.onCategoryClicked(getItem());
                }
            }
        });
    }

    public interface CategoryHolderListener {
        void onCategoryClicked(Category category);
    }
}