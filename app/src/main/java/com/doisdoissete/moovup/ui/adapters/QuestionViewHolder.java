package com.doisdoissete.moovup.ui.adapters;

import android.support.v7.widget.CardView;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.doisdoissete.domain.entity.Question;
import com.doisdoissete.moovup.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 11/3/15.
 */
@LayoutId(R.layout.card_feed)
public class QuestionViewHolder extends ItemViewHolder<Question> {

    @ViewId(R.id.ripple)
    MaterialRippleLayout card;

    @ViewId(R.id.img_feed_photo)
    CircleImageView imgPhoto;

    @ViewId(R.id.lbl_feed_name)
    TextView lblName;

    @ViewId(R.id.lbl_feed_date)
    TextView lblDate;

    @ViewId(R.id.lbl_feed_category)
    TextView lblCategory;

    @ViewId(R.id.lbl_feed_content)
    TextView lblContent;

    @ViewId(R.id.lbl_feed_comments)
    TextView lblComments;

    public QuestionViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Question item, PositionInfo positionInfo) {
        if (item.getUser().getProfileImage() != null && item.getUser().getProfileImage().getThumb() != null)
            Picasso.with(getContext()).load(item.getUser().getProfileImage().getThumb()).placeholder(R.drawable.user_placeholder).into(imgPhoto);

        lblName.setText(item.getUser().getFullName());
        lblCategory.setText(item.getCategory().getTitle());
        lblContent.setText(item.getBodyText());
        lblComments.setText(String.valueOf(item.getFavoritedCount()));

        DateFormat df = new DateFormat();
        lblDate.setText(df.format("dd/MM/yyyy", item.getCreatedAt()));
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionHolderListener listener = getListener(QuestionHolderListener.class);
                if (listener != null) {
                    listener.onQuestionSelected(getItem());
                }
            }
        });
    }

    public interface QuestionHolderListener {
        void onQuestionSelected(Question question);
    }
}