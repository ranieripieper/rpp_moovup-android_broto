package com.doisdoissete.moovup.ui.fragments.login;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.domain.entity.Career;
import com.doisdoissete.domain.entity.Hobby;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.SignUpPersonPresenter;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.SignUpPersonView;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.ganfra.materialspinner.MaterialSpinner;
import permissions.dispatcher.NeedsPermission;

/**
 * Created by broto on 9/18/15.
 */

//@RuntimePermissions
public class SignUpPersonFragment extends BaseView<SignUpPersonPresenter> implements
        Validator.ValidationListener,
        SignUpPersonView,
        ImageChooserListener {

    private static final String ARG_EMAIL = "param_email";
    private static final String ARG_PASSWORD = "param_password";

    @NotEmpty(messageResId = R.string.required_field)
    @Email(messageResId = R.string.invalid_email)
    @Bind(R.id.txt_profile_email)
    public EditText txtEmail;

    @Bind(R.id.img_profile_photo)
    public CircleImageView imgPhoto;

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.txt_profile_name)
    public EditText txtName;

    @Bind(R.id.txt_profile_family_name)
    public EditText txtFamilyName;

    @Bind(R.id.txt_profile_birthday)
    public EditText txtBirthday;

    @Bind(R.id.spinner_profile_job)
    public MaterialSpinner spinnerCareer;

    @Bind(R.id.spinner_profile_hobby)
    public MaterialSpinner spinnerHobby;

    private DatePickerDialog datePicker;

    private String email;
    private String password;

    // Photo Picker Stuff
    private ImageChooserManager imageChooserManager;
    private int chooserType;
    private String filePath;

    public SignUpPersonFragment() {
    }

    public static SignUpPersonFragment newInstance(String email, String password) {
        SignUpPersonFragment fragment = new SignUpPersonFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_EMAIL, email);
        bundle.putString(ARG_PASSWORD, password);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(ARG_EMAIL);
            password = getArguments().getString(ARG_PASSWORD);
        }

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (email.isEmpty()) {
            txtEmail.setVisibility(View.VISIBLE);
        } else {
            txtEmail.setText(email);
        }

        initDateField();
        initFocusFields();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_profile_input;
    }

    @Override
    public void onValidationSucceeded() {
        presenter.signUp(email, password,
                txtName.getText().toString(),
                txtFamilyName.getText().toString(),
                txtBirthday.getText().toString(),
                spinnerCareer.getSelectedItemPosition(),
                spinnerHobby.getSelectedItemPosition(),
                filePath);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }

    @OnClick(R.id.btn_profile_done)
    public void onDoneClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(this);
        validator.validate();
    }

    @OnClick(R.id.img_profile_photo)
    public void onPhotoClick() {
        presenter.onPhotoClick();
    }

    @Override
    public void showErrorDialog(String message) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.invalid)
                .content(message)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .icon(getResources().getDrawable(R.drawable.icn_error_alert))
                .show();
    }

    @Override
    public void showGoalActivity() {
        navigator.navigateToGoalActivity(getActivity(), true);
        getActivity().finish();
    }

    @Override
    public void renderCareers(List<Career> careers) {
        ArrayList<String> spinnerArray = new ArrayList<String>();
        for (Career career : careers) {
            spinnerArray.add(career.getTitle());
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        spinnerCareer.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void renderHobbies(List<Hobby> hobbies) {
        ArrayList<String> spinnerArray = new ArrayList<String>();
        for (Hobby hobby : hobbies) {
            spinnerArray.add(hobby.getTitle());
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        spinnerHobby.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                filePath = chosenImage.getFileThumbnail();
                Picasso.with(getActivity()).load(new File(chosenImage.getFileThumbnail())).resize(100, 100).into(imgPhoto);
            }
        });
    }

    @Override
    public void onError(String s) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getActivity()).load(R.drawable.icn_photo_placeholder).into(imgPhoto);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        }
    }

    @Override
    public void showMediaTypeChooser() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_photo_video_chooser);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.findViewById(R.id.choose_photo).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseImage();
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.take_picture).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                takePicture();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    private void chooseImage() {
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_PICK_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takePicture() {
        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_CAPTURE_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Should be called if for some reason the ImageChooserManager is null (Due
    // to destroying of activity for low memory situations)
    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    private void initDateField() {
        txtBirthday.setOnKeyListener(null);
        txtBirthday.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();

                if (MotionEvent.ACTION_UP == event.getAction()) {
                    buildDatePicker().show();
                }

                return true;
            }
        });
    }

    private void initFocusFields() {
        spinnerCareer.setOnTouchListener(focusListener);
        spinnerHobby.setOnTouchListener(focusListener);
    }

    View.OnTouchListener focusListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideKeyboard();
            return false;
        }
    };

    private DatePickerDialog buildDatePicker() {
        if (datePicker == null) {
            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            datePicker = new DatePickerDialog(getActivity(),
                    R.style.DialogTheme,
                    datePickerListener,
                    cal.get(Calendar.YEAR) - 18,
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            datePicker.setCancelable(false);
        }

        return datePicker;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            String year1 = String.valueOf(selectedYear);
            String month1 = String.valueOf(selectedMonth + 1);
            String day1 = String.valueOf(selectedDay);
            txtBirthday.setText(day1 + "/" + month1 + "/" + year1);
        }
    };
}