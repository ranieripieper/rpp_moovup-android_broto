package com.doisdoissete.moovup.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.navigation.Navigator;
import com.doisdoissete.moovup.ui.custom.ScrollThroughRecyclerView;

import uk.co.ribot.easyadapter.EasyRecyclerAdapter;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_category_section)
public class CategoryGroupViewHolder extends ItemViewHolder<Category> implements CategoryViewHolder.CategoryHolderListener{

    @ViewId(R.id.lbl_row_category_title)
    TextView lblTitle;

    @ViewId(R.id.lbl_row_category_subtitle)
    TextView lblSubtitle;

    @ViewId(R.id.lbl_row_category_open)
    TextView lblOpen;

    @ViewId(R.id.lbl_row_category_close)
    TextView lblClose;

    @ViewId(R.id.recycler_row_category_children)
    ScrollThroughRecyclerView recyclerView;

    public CategoryGroupViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Category item, PositionInfo positionInfo) {
        lblTitle.setText(item.getTitle());
        lblSubtitle.setText(String.valueOf(item.getChildren().size()) + " " + getContext().getString(R.string.items));

        LinearLayoutManager layoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.enableVersticleScroll(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new EasyRecyclerAdapter<>(
                getContext(),
                CategoryViewHolder.class,
                item.getChildren(),
                this));
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        lblOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblOpen.setVisibility(View.GONE);
                lblClose.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });

        lblClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblOpen.setVisibility(View.VISIBLE);
                lblClose.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onCategoryClicked(Category category) {
        new Navigator().navigateToGoalScheduleFragment(getContext(), category);
    }
}