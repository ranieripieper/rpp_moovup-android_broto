package com.doisdoissete.moovup.ui.views;

import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.domain.entity.Interest;

import java.util.List;

/**
 * Created by broto on 10/30/15.
 */
public interface InterestView extends LoadingView{

    void renderCategories(List<Category> categoryList);
}
