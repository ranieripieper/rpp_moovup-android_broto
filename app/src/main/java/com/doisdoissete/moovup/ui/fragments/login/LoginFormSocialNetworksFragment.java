package com.doisdoissete.moovup.ui.fragments.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.LoginSocialPresenter;
import com.doisdoissete.moovup.ui.activities.LoginGoogleActivity;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.LoginSocialNetWorksView;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLoginListener;

import java.util.List;

import butterknife.OnClick;

/**
 * Created by broto on 9/14/15.
 */
public class LoginFormSocialNetworksFragment extends BaseView<LoginSocialPresenter> implements LoginSocialNetWorksView {

    SimpleFacebook mSimpleFacebook;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_form_social_networks;
    }

    @OnClick(R.id.btn_socialnetwork_facebook)
    public void onFacebookClick(){
        presenter.onFacebookClick();
    }

    @OnClick(R.id.btn_socialnetwork_google)
    public void onGoogleClick(){
        presenter.onGoogleClick();
    }

    @Override
    public void showErrorDialog(String message) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.invalid)
                .content(message)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .icon(getResources().getDrawable(R.drawable.icn_error_alert))
                .show();
    }

    @Override
    public void showErrorDialog(@StringRes int messageRes) {
        showErrorDialog(getString(messageRes));
    }

    @Override
    public void finishActivity() {
        getActivity().finish();
    }

    @Override
    public void showMainActivity() {
        navigator.navigateToMainActivity(getActivity());
    }

    @Override
    public void showGoalActivity() {
        navigator.navigateToGoalActivity(getActivity(), true);
    }

    @Override
    public void showGooglePlusActivity() {
        navigator.navigateToGooglePlusActivity(this, LoginGoogleActivity.GOOGLE_PLUS_RESULT);
    }

    @Override
    public void getFacebookToken() {
        mSimpleFacebook = SimpleFacebook.getInstance();

        if (mSimpleFacebook.isLogin()) {
            presenter.loginFacebook(mSimpleFacebook.getAccessToken().getToken());
        } else {
            mSimpleFacebook.login(new OnLoginListener() {
                @Override
                public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                    presenter.loginFacebook(accessToken);
                }

                @Override
                public void onCancel() {
                    presenter.loginFacebookError();
                }

                @Override
                public void onFail(String reason) {
                    presenter.loginFacebookError();
                }

                @Override
                public void onException(Throwable throwable) {
                    presenter.loginFacebookError();
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LoginGoogleActivity.GOOGLE_PLUS_RESULT) {
            if (data != null) {
                String token = data.getStringExtra(LoginGoogleActivity.PARAM_GOOGLE_PLUS_TOKEN);

                presenter.loginGoogle(token);
            } else {
                presenter.loginGoogleError();
            }
        }
    }
}
