package com.doisdoissete.moovup.ui.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.navigation.Navigator;
import com.doisdoissete.moovup.ui.fragments.BaseFragment;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Nullable
    @Bind(R.id.navigation_view)
    NavigationView mNavigationView;

    @Nullable
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @Inject
    Navigator navigator;

    ActionBarDrawerToggle mDrawerToggle;

    protected abstract int getLayoutResource();
    protected abstract int getLayoutFragmentContainer();
    protected abstract Fragment getMainFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResource());

        this.getApplicationComponent().inject(this);
        ButterKnife.bind(this);

        initToolbar();
        initDrawerToggle();
        initBackStackListener();
        iniNavigationView();

        addFragment(getMainFragment());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }


    /**
     * Calligraphy
     *
     * @param newBase
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getFragmentManager().getBackStackEntryCount() <= 1) {
                    if (mDrawerLayout != null)
                        mDrawerLayout.openDrawer(GravityCompat.START);
                } else {
                    getFragmentManager().popBackStack();
                }

                return true;
            default:
                break;
        }
        return mDrawerToggle.onOptionsItemSelected(item);
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param fragment The fragment to be added.
     */
    public void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.enter_from_right, R.anim.exit_to_left, R.anim.exit_to_right);
        fragmentTransaction.add(getLayoutFragmentContainer(), fragment);
        fragmentTransaction.addToBackStack(null).commit();

        if (getFragmentManager().getBackStackEntryCount() > 0) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    /**
     * Get the Main Application component for dependency injection.
     */
    protected ApplicationComponent getApplicationComponent() {
        return ((MoovUpApplication) getApplication()).getApplicationComponent();
    }

    public
    @Nullable
    Toolbar getToolbar() {
        return mToolbar;
    }

    private void initToolbar() {
        if (mToolbar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mToolbar.setElevation(getResources().getDimensionPixelSize(R.dimen.action_bar_elevation));
            }
            setSupportActionBar(mToolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(false);
                getSupportActionBar().setTitle(getString(R.string.app_name));
            }
        }
    }

    private void initDrawerToggle() {
        if (mDrawerLayout != null) {
            if (mNavigationView != null) {
                mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        mDrawerLayout.closeDrawers();
                        menuItem.setChecked(true);

                        return true;
                    }
                });
            }

            mDrawerToggle = new ActionBarDrawerToggle(
                    this,
                    mDrawerLayout,
                    R.string.action_drawer_open,
                    R.string.action_drawer_close
            );

            mDrawerLayout.setDrawerListener(mDrawerToggle); // REQUIRED FOR ARROW ANIMATION. Fml.
            mDrawerToggle.syncState();
        }
    }

    private void iniNavigationView() {
        if (getSupportActionBar() != null) {
            if (mNavigationView == null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    private void initBackStackListener() {
        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                BaseFragment fragment = (BaseFragment) getFragmentManager().findFragmentById(getLayoutFragmentContainer());

                switch (getFragmentManager().getBackStackEntryCount()) {
                    case 0: // Pressing back on first fragment in the pile
                        finish();
                        break;
                    case 1: // Show Hamburger menu if fragment is the first in pile
                        if (mNavigationView != null) {
                            mDrawerToggle.setDrawerIndicatorEnabled(true);
                        } else {
                            if (getSupportActionBar() != null)
                                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                        }

                        fragment.onResumeFromBackstack();
                        break;
                    default: // Shows back arrow to all other fragments
                        if (getSupportActionBar() != null) {
                            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                            getSupportActionBar().setHomeButtonEnabled(true);
                            mDrawerToggle.setDrawerIndicatorEnabled(false);
                        }
                        fragment.onResumeFromBackstack();
                        break;
                }
            }
        });
    }
}
