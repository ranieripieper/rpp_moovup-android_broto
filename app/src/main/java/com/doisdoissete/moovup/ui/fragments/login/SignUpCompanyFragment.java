package com.doisdoissete.moovup.ui.fragments.login;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.domain.entity.CommercialActivity;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.presenters.SignUpCompanyPresenter;
import com.doisdoissete.moovup.ui.views.BaseView;
import com.doisdoissete.moovup.ui.views.SignUpCompanyView;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by broto on 9/18/15.
 */
public class SignUpCompanyFragment extends BaseView<SignUpCompanyPresenter> implements
        Validator.ValidationListener,
        SignUpCompanyView,
        ImageChooserListener {

    private static final String ARG_EMAIL = "param_email";
    private static final String ARG_PASSWORD = "param_password";

    @Bind(R.id.img_profile_photo)
    public CircleImageView imgPhoto;

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.txt_profile_company_name)
    public EditText txtCompanyName;

    @Bind(R.id.txt_profile_foudation_date)
    public EditText txtFoundationDate;

    @Bind(R.id.spinner_profile_job)
    public Spinner spinnerCommercialActivity;

    private DatePickerDialog datePicker;

    private String email;
    private String password;

    // Photo Picker Stuff
    private ImageChooserManager imageChooserManager;
    private int chooserType;
    private String filePath;

    public SignUpCompanyFragment() {
    }

    public static SignUpCompanyFragment newInstance(String email, String password) {
        SignUpCompanyFragment fragment = new SignUpCompanyFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_EMAIL, email);
        bundle.putString(ARG_PASSWORD, password);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(ARG_EMAIL);
            password = getArguments().getString(ARG_PASSWORD);
        }

        this.getComponent(ApplicationComponent.class).inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDateField();
        initFocusFields();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_profile_input_company;
    }

    @OnClick(R.id.btn_profile_submit)
    public void onSubmitClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(this);
        validator.validate();
    }

    @OnClick(R.id.img_profile_photo)
    public void onPhotoClick() {
        showMediaTypeChooser();
    }

    @Override
    public void showErrorDialog(String message) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.invalid)
                .content(message)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .icon(getResources().getDrawable(R.drawable.icn_error_alert))
                .show();
    }

    @Override
    public void showGoalActivity() {
        navigator.navigateToGoalActivity(getActivity(), true);
    }

    @Override
    public void renderCommercialActivities(List<CommercialActivity> commercialActivityList){
        ArrayList<String> spinnerArray = new ArrayList<String>();
        for (CommercialActivity commercialActivity : commercialActivityList) {
            spinnerArray.add(commercialActivity.getTitle());
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        spinnerCommercialActivity.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onValidationSucceeded() {
        // TODO: Send righ job and hobby ids
        presenter.signUp(email, password,
                txtCompanyName.getText().toString(),
                txtFoundationDate.getText().toString(),
                spinnerCommercialActivity.getSelectedItemPosition(),
                filePath);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                filePath = chosenImage.getFilePathOriginal();
                Picasso.with(getActivity()).load(new File(chosenImage.getFileThumbnail())).into(imgPhoto);
            }
        });
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        }
    }

    private void showMediaTypeChooser() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_photo_video_chooser);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.findViewById(R.id.choose_photo).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseImage();
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.take_picture).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                takePicture();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void chooseImage() {
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_PICK_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takePicture() {
        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_CAPTURE_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Should be called if for some reason the ImageChooserManager is null (Due
    // to destroying of activity for low memory situations)
    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    private void initDateField() {
        txtFoundationDate.setOnKeyListener(null);
        txtFoundationDate.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();

                if (MotionEvent.ACTION_UP == event.getAction()) {
                    buildDatePicker().show();
                }

                return true;
            }
        });
    }

    private void initFocusFields() {
        spinnerCommercialActivity.setOnTouchListener(focusListener);
    }

    View.OnTouchListener focusListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideKeyboard();
            return false;
        }
    };

    private DatePickerDialog buildDatePicker() {
        if (datePicker == null) {
            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            datePicker = new DatePickerDialog(getActivity(),
                    R.style.DialogTheme,
                    datePickerListener,
                    cal.get(Calendar.YEAR) - 18,
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));
            datePicker.setCancelable(false);
        }

        return datePicker;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            String year1 = String.valueOf(selectedYear);
            String month1 = String.valueOf(selectedMonth + 1);
            String day1 = String.valueOf(selectedDay);
            txtFoundationDate.setText(day1 + "/" + month1 + "/" + year1);
        }
    };
}
