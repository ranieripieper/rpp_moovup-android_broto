package com.doisdoissete.moovup.ui.custom.validation;

import com.doisdoissete.moovup.R;
import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by broto on 10/12/15.
 */

@ValidateUsing(AtLeastOneNumberRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface AtLeastOneNumber {

    int sequence()      default -1;
    int messageResId()  default R.string.password_at_least_one_number;
    String message()    default "";
}
