package com.doisdoissete.moovup.ui.views;

import com.doisdoissete.domain.entity.CommercialActivity;

import java.util.List;

/**
 * Created by broto on 9/22/15.
 */
public interface SignUpCompanyView extends LoadingView{

    void showErrorDialog(String message);
    void showGoalActivity();
    void renderCommercialActivities(List<CommercialActivity> commercialActivityList);
}
