package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.domain.entity.Question;
import com.doisdoissete.domain.entity.UserNew;
import com.doisdoissete.domain.events.OnFeedErrorEvent;
import com.doisdoissete.domain.events.OnFeedSuccessEvent;
import com.doisdoissete.domain.interactor.GetFeedUseCase;
import com.doisdoissete.moovup.ui.views.FeedView;

import java.util.List;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;

/**
 * Created by broto on 10/27/15.
 */
public class FeedPresenter extends BasePresenter<FeedView> {

    private GetFeedUseCase useCase;

    public FeedPresenter(EventBus bus, GetFeedUseCase useCase) {
        super(bus);
        this.useCase = useCase;
    }

    @Override
    public void resume() {
        super.resume();

        view.showLoadingView();
        loadData();
    }

    public void onRefresh() {
        loadData();
    }

    private void loadData() {
        useCase.getFeed();
    }

    @DebugLog
    public void onEvent(OnFeedSuccessEvent event) {
        List<Question> questions = event.getQuestionList();
        List<UserNew> users = event.getUserList();
        List<Category> categories = event.getCategoryList();

        for (Question question : questions) {
            for (UserNew user : users) {
                if (user.getId() == question.getUserId()) {
                    question.setUser(user);
                    break;
                }
            }
        }

        for (Question question : questions) {
            for (Category category : categories) {
                if (category.getId() == question.getCategoryId()) {
                    question.setCategory(category);
                    break;
                }
            }
        }

        view.renderQuestion(event.getQuestionList());
        view.hideLoadingView();
    }

    public void onEvent(OnFeedErrorEvent event) {
        view.hideLoadingView();
        view.showErrorMessage();
    }
}
