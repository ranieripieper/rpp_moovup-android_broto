package com.doisdoissete.moovup.presenters;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 6/29/15.
 */
public abstract class BasePresenter<T> {

    protected T view;
    protected EventBus bus;

    public BasePresenter(EventBus bus) {
        this.bus = bus;
    }

    public void attachView(T view){
        this.view = view;
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onResume() method.
     */
    public void resume() {
        if (!bus.isRegistered(this)) {
            bus.register(this);
        }
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onPause() method.
     */
    public void pause() {
        if (bus.isRegistered(this)) {
            bus.unregister(this);
        }
    }

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     */
    public void destroy() {
        if (bus.isRegistered(this)) {
            bus.unregister(this);
        }
    }
}
