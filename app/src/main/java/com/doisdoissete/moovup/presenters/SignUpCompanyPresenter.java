package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.entity.CommercialActivity;
import com.doisdoissete.domain.entity.Company;
import com.doisdoissete.domain.entity.mapper.CommercialActivityMapper;
import com.doisdoissete.domain.events.OnCommercialActivityErrorEvent;
import com.doisdoissete.domain.events.OnCommercialActivitySuccessEvent;
import com.doisdoissete.domain.events.OnSignUpErrorEvent;
import com.doisdoissete.domain.events.OnSignUpSuccessEvent;
import com.doisdoissete.domain.interactor.GetCommercialActivitiesUseCase;
import com.doisdoissete.domain.interactor.SignUpCompanyUserCase;
import com.doisdoissete.moovup.ui.views.SignUpCompanyView;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 9/24/15.
 */
public class SignUpCompanyPresenter extends BasePresenter<SignUpCompanyView>{

    protected SignUpCompanyUserCase useCase;
    protected CommercialActivityMapper mapper;
    protected GetCommercialActivitiesUseCase commercialActivitiesUseCase;

    private List<CommercialActivity> commercialActivityList;

    public SignUpCompanyPresenter(EventBus bus, SignUpCompanyUserCase useCase, CommercialActivityMapper mapper, GetCommercialActivitiesUseCase commercialActivitiesUseCase) {
        super(bus);
        this.useCase = useCase;
        this.mapper = mapper;
        this.commercialActivitiesUseCase = commercialActivitiesUseCase;
    }

    @Override
    public void resume() {
        super.resume();

        commercialActivitiesUseCase.getAllComercialActivities();
    }

    // TODO: Upload photo
    public void signUp(String email, String password, String companyName, String foudationDate, int selectedItemPosition, String photoPath) {
        view.showLoadingView();
        useCase.signUp(new Company(email, password, password, photoPath, companyName, foudationDate, commercialActivityList.get(selectedItemPosition).getId()));
    }

    public void onEventMainThread(OnSignUpSuccessEvent event) {
        view.hideLoadingView();
        view.showGoalActivity();
    }

    public void onEventMainThread(OnSignUpErrorEvent event) {
        StringBuilder builder = new StringBuilder();
        builder.append("Error creating account: \n ");

        for (String s : event.getErrors()) {
            builder.append(s).append("\n");
        }

        view.hideLoadingView();
        view.showErrorDialog(builder.toString());
    }

    public void onEventMainThread(OnCommercialActivitySuccessEvent event) {
        commercialActivityList = mapper.map(event.getCommercialActivityEntityList());
        view.renderCommercialActivities(commercialActivityList);
    }

    public void onEventMainThread(OnCommercialActivityErrorEvent event) {
        view.showErrorDialog("Error retrieving Commercial Activities.");
    }
}
