package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.entity.Goal;
import com.doisdoissete.domain.entity.mapper.GoalMapper;
import com.doisdoissete.domain.events.OnGoalAddErrorEvent;
import com.doisdoissete.domain.events.OnGoalAddSuccessEvent;
import com.doisdoissete.domain.interactor.GetLoggedUserUseCase;
import com.doisdoissete.domain.interactor.NewGoalUseCase;
import com.doisdoissete.moovup.ui.views.GoalWeightView;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 10/16/15.
 */
public class GoalWeightPresenter extends BasePresenter<GoalWeightView> {

    private NewGoalUseCase useCase;
    private GoalMapper mapper;

    public GoalWeightPresenter(EventBus bus, NewGoalUseCase useCase, GoalMapper mapper) {
        super(bus);
        this.useCase = useCase;
        this.mapper = mapper;
    }

    public void saveGoal(Goal goal) {
        view.showLoadingView();
        useCase.addGoal(mapper.map(goal));
    }

    public void onEvent(OnGoalAddSuccessEvent event) {
        view.hideLoadingView();
        view.showSuccessMessage();
    }

    public void onEvent(OnGoalAddErrorEvent event) {
        view.hideLoadingView();
        view.showErrorMessage();
    }

}
