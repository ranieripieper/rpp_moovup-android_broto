package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.events.OnForgotPasswordErrorEvent;
import com.doisdoissete.domain.events.OnForgotPasswordSuccessEvent;
import com.doisdoissete.domain.events.OnLoginErrorEvent;
import com.doisdoissete.domain.events.OnLoginSuccessEvent;
import com.doisdoissete.domain.interactor.ForgotPasswordUseCase;
import com.doisdoissete.domain.interactor.LoginEmailUseCase;
import com.doisdoissete.moovup.ui.views.LoginView;

import javax.inject.Singleton;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 9/16/15.
 */
@Singleton
public class LoginEmailPresenter extends BasePresenter<LoginView> {

    private LoginEmailUseCase loginEmailUseCase;
    private ForgotPasswordUseCase forgotPasswordUseCase;

    public LoginEmailPresenter(EventBus bus, LoginEmailUseCase loginEmailUseCase, ForgotPasswordUseCase forgotPasswordUseCase) {
        super(bus);

        this.loginEmailUseCase = loginEmailUseCase;
        this.forgotPasswordUseCase = forgotPasswordUseCase;
    }

    public void signIn(String email, String password) {
        view.showLoadingView();
        loginEmailUseCase.login(email, password);
    }

    public void forgotPasswordClicked(String email) {
        view.showLoadingView();
        forgotPasswordUseCase.requestPassword(email);
    }

    public void onEventMainThread(OnLoginSuccessEvent event) {
        view.hideLoadingView();
        view.showMainActivity();
        view.finishActivity();
    }

    public void onEventMainThread(OnLoginErrorEvent event) {
        view.hideLoadingView();
        view.showErrorDialog("Login invalid.");
        view.emptyFields();
    }

    public void onEventMainThread(OnForgotPasswordSuccessEvent event) {
        view.hideLoadingView();
        view.showSuccessDialog("Password sent to your email address.");
        view.emptyFields();
    }

    public void onEventMainThread(OnForgotPasswordErrorEvent event) {
        view.hideLoadingView();
        view.showErrorDialog("Error requesting password reset.");
        view.emptyFields();
    }
}
