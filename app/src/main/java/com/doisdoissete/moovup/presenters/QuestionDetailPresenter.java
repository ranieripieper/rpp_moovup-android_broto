package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.entity.Question;
import com.doisdoissete.domain.events.OnCategoriesSuccessEvent;
import com.doisdoissete.moovup.ui.views.QuestionDetailView;

import javax.inject.Singleton;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 11/6/15.
 */
@Singleton
public class QuestionDetailPresenter extends BasePresenter<QuestionDetailView> {

    private Question question;

    public QuestionDetailPresenter(EventBus bus) {
        super(bus);
    }

    public void onEvent(OnCategoriesSuccessEvent event){}

    public void setQuestion(Question question) {
        this.question = question;

        view.renderQuestion(question);
    }
}
