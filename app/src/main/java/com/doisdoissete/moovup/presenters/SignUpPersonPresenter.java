package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.entity.Career;
import com.doisdoissete.domain.entity.Hobby;
import com.doisdoissete.domain.entity.Person;
import com.doisdoissete.domain.entity.mapper.CareersMapper;
import com.doisdoissete.domain.entity.mapper.HobbyMapper;
import com.doisdoissete.domain.events.OnCareersArrivedEvent;
import com.doisdoissete.domain.events.OnCareersErrorEvent;
import com.doisdoissete.domain.events.OnHobbiesArrivedEvent;
import com.doisdoissete.domain.events.OnHobbiesErrorEvent;
import com.doisdoissete.domain.events.OnSignUpErrorEvent;
import com.doisdoissete.domain.events.OnSignUpSuccessEvent;
import com.doisdoissete.domain.interactor.GetCareersUseCase;
import com.doisdoissete.domain.interactor.GetHobbiesUseCase;
import com.doisdoissete.domain.interactor.SignUpPersonUserCase;
import com.doisdoissete.moovup.ui.views.SignUpPersonView;

import java.util.List;

import javax.inject.Singleton;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 9/18/15.
 */
@Singleton
public class SignUpPersonPresenter extends BasePresenter<SignUpPersonView> {

    protected SignUpPersonUserCase useCase;
    protected CareersMapper careersMapper;
    protected HobbyMapper hobbyMapper;
    protected GetCareersUseCase careersUseCase;
    protected GetHobbiesUseCase hobbiesUseCase;

    private List<Career> careers;
    private List<Hobby> hobbies;

    public SignUpPersonPresenter(EventBus bus,
                                 CareersMapper mapper,
                                 HobbyMapper hobbyMapper,
                                 SignUpPersonUserCase useCase,
                                 GetCareersUseCase careersUseCase,
                                 GetHobbiesUseCase hobbiesUseCase) {
        super(bus);
        this.careersMapper = mapper;
        this.hobbyMapper = hobbyMapper;
        this.useCase = useCase;
        this.careersUseCase = careersUseCase;
        this.hobbiesUseCase = hobbiesUseCase;
    }

    @Override
    public void resume() {
        super.resume();

        careersUseCase.getCareers(20);
        hobbiesUseCase.getAllHobbies();
    }

    public void signUp(String email, String password, String name, String lastName, String birthdate, int carrierId, int hobbyId, String photoPath) {
        view.showLoadingView();

        Person person = new Person();
        person.setFirstName(name);
        person.setLastName(lastName);
        person.setBirthday(birthdate);
        // TODO: Check Gender default value
        person.setGender("male");
        person.setEmail(email);
        person.setPassword(password);
        person.setPasswordConfirmation(password);
        person.setCarrerId(careers.get(carrierId - 1).getId());
        person.setHobbyId(hobbies.get(hobbyId - 1).getId());
        person.setPhotoPath(photoPath);

        useCase.signUp(person);
    }

    public void onPhotoClick() {
        view.showMediaTypeChooser();
    }

    public void onEventMainThread(OnSignUpSuccessEvent event) {
        view.hideLoadingView();
        view.showGoalActivity();
    }

    public void onEventMainThread(OnSignUpErrorEvent event) {
        view.hideLoadingView();
        StringBuilder builder = new StringBuilder();
        builder.append("Error registering your account!");
        builder.append("\n");
        builder.append("\n");

        for (String error : event.getErrors()) {
            builder.append(error);
            builder.append(".");
            builder.append("\n");
        }

        view.showErrorDialog(builder.toString());
    }

    public void onEventMainThread(OnCareersArrivedEvent event) {
        careers = careersMapper.map(event.getCarrerEntityList());
        view.renderCareers(careers);
    }

    public void onEventMainThread(OnCareersErrorEvent event) {
        view.showErrorDialog("Error retrieving Careers. Try Again later please.");
    }

    public void onEventMainThread(OnHobbiesArrivedEvent event) {
        hobbies = hobbyMapper.map(event.getHobbyEntityList());
        view.renderHobbies(hobbies);
    }

    public void onEventMainThread(OnHobbiesErrorEvent event) {
        view.showErrorDialog("Error retrieving Carriers. Try Again later please.");
    }
}
