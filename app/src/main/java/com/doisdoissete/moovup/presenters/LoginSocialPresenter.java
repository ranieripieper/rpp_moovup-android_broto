package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.events.OnSocialLoginErrorEvent;
import com.doisdoissete.domain.events.OnSocialLoginSuccessEvent;
import com.doisdoissete.domain.interactor.LoginFacebookUseCase;
import com.doisdoissete.domain.interactor.LoginGoogleUseCase;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.ui.views.LoginSocialNetWorksView;

import javax.inject.Singleton;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 9/18/15.
 */
@Singleton
public class LoginSocialPresenter extends BasePresenter<LoginSocialNetWorksView> {

    private LoginFacebookUseCase facebookUseCase;
    private LoginGoogleUseCase googleUseCase;

    public LoginSocialPresenter(EventBus bus, LoginFacebookUseCase facebookUseCase, LoginGoogleUseCase googleUseCase) {
        super(bus);
        this.facebookUseCase = facebookUseCase;
        this.googleUseCase = googleUseCase;
    }

    public void onGoogleClick() {
        view.showLoadingView();
        view.showGooglePlusActivity();
    }

    public void onFacebookClick() {
        view.showLoadingView();
        view.getFacebookToken();
    }

    public void loginGoogle(String token) {
        googleUseCase.login(token);
    }

    public void loginFacebook(String token) {
        view.showLoadingView();
        facebookUseCase.login(token);
    }

    public void loginGoogleError() {
        view.hideLoadingView();
        view.showErrorDialog(R.string.error_login_in_with_google_plus);
    }

    public void loginFacebookError() {
        view.hideLoadingView();
        view.showErrorDialog(R.string.error_login_in_with_facebook);
    }

    /**
     * Issued by {@link LoginFacebookUseCase#login(String)} login} method as well as
     * {@link LoginGoogleUseCase#login(String)} login}
     *
     * @param event
     */
    public void onEventMainThread(OnSocialLoginSuccessEvent event) {
        view.hideLoadingView();

        if (event.getUser().isNewUser()) {
            view.showGoalActivity();
        } else {
            view.showMainActivity();
        }

        view.finishActivity();
    }

    /**
     * Issued by {@link LoginFacebookUseCase#login(String)} login} method as well as
     * {@link LoginGoogleUseCase#login(String)} login}
     *
     * @param event
     */
    public void onEventMainThread(OnSocialLoginErrorEvent event) {
        StringBuilder builder = new StringBuilder();
        builder.append("Error creating account: \n ");

        if (event != null && event.getErrors() != null) {
            for (String s : event.getErrors()) {
                builder.append(s).append("\n");
            }
        }

        view.hideLoadingView();
        view.showErrorDialog(builder.toString());
    }
}
