package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.entity.Interest;
import com.doisdoissete.domain.entity.mapper.InterestMapper;
import com.doisdoissete.domain.events.OnCategoriesErrorEvent;
import com.doisdoissete.domain.events.OnCategoriesSuccessEvent;
import com.doisdoissete.domain.events.OnInterestErrorEvent;
import com.doisdoissete.domain.events.OnInterestSuccessEvent;
import com.doisdoissete.domain.interactor.AddInterestUseCase;
import com.doisdoissete.domain.interactor.GetCategoriesUseCase;
import com.doisdoissete.domain.interactor.RemoveInterestUseCase;
import com.doisdoissete.moovup.ui.views.InterestView;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 10/30/15.
 */
public class InterestPresenter extends BasePresenter<InterestView> {

    private GetCategoriesUseCase categoriesUseCase;
    private AddInterestUseCase addInterestUseCase;
    private RemoveInterestUseCase removeInterestUseCase;
    private InterestMapper mapper;

    public InterestPresenter(EventBus bus, GetCategoriesUseCase categoriesUseCase, AddInterestUseCase addInterestUseCase, RemoveInterestUseCase removeInterestUseCase, InterestMapper mapper) {
        super(bus);
        this.categoriesUseCase = categoriesUseCase;
        this.addInterestUseCase = addInterestUseCase;
        this.removeInterestUseCase = removeInterestUseCase;
        this.mapper = mapper;
    }

    @Override
    public void resume() {
        super.resume();

        view.showLoadingView();
        categoriesUseCase.getCategories();
    }

    public void onEvent(OnInterestSuccessEvent event) {

    }

    public void onEvent(OnInterestErrorEvent event) {

    }

    public void onEvent(OnCategoriesSuccessEvent event) {
        view.hideLoadingView();
        view.renderCategories(event.getCategories());
    }

    public void onEvent(OnCategoriesErrorEvent event) {
        view.hideLoadingView();
    }

    public void handleInterest(Interest interest) {
        if (interest.isActive()) {
            addInterestUseCase.add(mapper.map(interest));
        } else {
            removeInterestUseCase.remove(interest.getCategoryId());
        }
    }
}
