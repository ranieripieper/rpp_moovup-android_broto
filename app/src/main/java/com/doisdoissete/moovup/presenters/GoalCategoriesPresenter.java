package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.events.OnCategoriesErrorEvent;
import com.doisdoissete.domain.events.OnCategoriesSuccessEvent;
import com.doisdoissete.domain.events.OnSocialLoginErrorEvent;
import com.doisdoissete.domain.interactor.GetCategoriesUseCase;
import com.doisdoissete.moovup.ui.views.GoalCategoriesView;

import javax.inject.Singleton;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 10/13/15.
 */
@Singleton
public class GoalCategoriesPresenter extends BasePresenter<GoalCategoriesView> {

    private GetCategoriesUseCase useCase;

    public GoalCategoriesPresenter(EventBus bus, GetCategoriesUseCase useCase) {
        super(bus);
        this.useCase = useCase;
    }

    @Override
    public void resume() {
        super.resume();

        useCase.getCategories();
    }

    public void onEventMainThread(OnCategoriesSuccessEvent event) {
       view.renderGoals(event.getCategories());
    }

    public void onEventMainThread(OnCategoriesErrorEvent event) {

    }


}
