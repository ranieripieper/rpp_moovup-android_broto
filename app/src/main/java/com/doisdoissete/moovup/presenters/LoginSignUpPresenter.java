package com.doisdoissete.moovup.presenters;

import com.doisdoissete.domain.events.OnEmailDoesntExistEvent;
import com.doisdoissete.domain.events.OnEmailExistEvent;
import com.doisdoissete.domain.interactor.CheckEmailUseCase;
import com.doisdoissete.moovup.ui.views.LoginSignUpView;

import de.greenrobot.event.EventBus;

/**
 * Created by broto on 10/13/15.
 */
public class LoginSignUpPresenter extends BasePresenter<LoginSignUpView> {

    private CheckEmailUseCase emailUseCase;

    public LoginSignUpPresenter(EventBus bus, CheckEmailUseCase emailUseCase) {
        super(bus);
        this.emailUseCase = emailUseCase;
    }

    public void isEmailRegistered(String email) {
        emailUseCase.checkEmail(email);
    }

    public void onEventMainThread(OnEmailExistEvent event) {
        view.hideEmailLoading();
        view.setInvalidForm();
    }

    public void onEventMainThread(OnEmailDoesntExistEvent event) {
        view.hideEmailLoading();
        view.setValidForm();
    }

    public boolean isValidEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
