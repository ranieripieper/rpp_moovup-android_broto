package com.doisdoissete.moovup;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.doisdoissete.moovup.di.components.ApplicationComponent;
import com.doisdoissete.moovup.di.components.DaggerApplicationComponent;
import com.doisdoissete.moovup.di.modules.AndroidModule;
import com.doisdoissete.moovup.ui.custom.validation.AtLeastOneLetter;
import com.doisdoissete.moovup.ui.custom.validation.AtLeastOneNumber;
import com.mobsandgeeks.saripaar.Validator;
import com.squareup.leakcanary.RefWatcher;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

import net.danlew.android.joda.JodaTimeAndroid;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by broto on 9/11/15.
 */
public class MoovUpApplication extends Application {

    private ApplicationComponent applicationComponent;

    public static RefWatcher getRefWatcher(Context context) {
        MoovUpApplication application = (MoovUpApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    protected RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        initInjector();
        initLoggers();
        initCalligraphy();
        initFacebook();
        initRealm();
        initValidators();
        initJodaTime();

        refWatcher = installLeakCanary();
    }

    private void initJodaTime() {
        JodaTimeAndroid.init(this);
    }

    private void initValidators() {
        Validator.registerAnnotation(AtLeastOneLetter.class);
        Validator.registerAnnotation(AtLeastOneNumber.class);
    }

    private void initCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Calibri.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }

    private void initRealm() {
        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);
    }

    /**
     *  Dagger 2 Component init
     */
    private void initInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
    }

    /**
     *  Init Leak Canary - Memory Leak tool
     * @return
     */
    protected RefWatcher installLeakCanary() {
        return RefWatcher.DISABLED;
    }

    private void initFacebook() {
        // Facebook Permissions
        Permission[] permissions = new Permission[] {
                Permission.EMAIL};

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getString(R.string.app_id))
                .setNamespace("moovup_test")
                .setPermissions(permissions)
                .build();
        SimpleFacebook.setConfiguration(configuration);
    }

    /**
     * Timber init
     */
    private void initLoggers() {
        Timber.plant(new Timber.Tree() {
            @Override
            protected void log(int priority, String tag, String message, Throwable t) {
                if (priority != Log.VERBOSE && priority != Log.DEBUG) {
                    Log.println(priority, tag, message);
                }
            }
        });
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
