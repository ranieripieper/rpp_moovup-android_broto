package com.doisdoissete.moovup.navigation;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.doisdoissete.domain.entity.Category;
import com.doisdoissete.domain.entity.Goal;
import com.doisdoissete.domain.entity.Question;
import com.doisdoissete.moovup.ui.activities.BaseActivity;
import com.doisdoissete.moovup.ui.activities.GoalActivity;
import com.doisdoissete.moovup.ui.activities.LoginActivity;
import com.doisdoissete.moovup.ui.activities.LoginGoogleActivity;
import com.doisdoissete.moovup.ui.activities.MainActivity;
import com.doisdoissete.moovup.ui.fragments.goals.InterestFragment;
import com.doisdoissete.moovup.ui.fragments.goals.GoalCategoriesFragment;
import com.doisdoissete.moovup.ui.fragments.goals.GoalScheduleFragment;
import com.doisdoissete.moovup.ui.fragments.goals.GoalWeightFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginIntroFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginSignInFragment;
import com.doisdoissete.moovup.ui.fragments.login.LoginSignUpFragment;
import com.doisdoissete.moovup.ui.fragments.login.ProfileFragment;
import com.doisdoissete.moovup.ui.fragments.login.SignUpCompanyFragment;
import com.doisdoissete.moovup.ui.fragments.login.TermsFragment;
import com.doisdoissete.moovup.ui.fragments.questions.FeedFragment;
import com.doisdoissete.moovup.ui.fragments.questions.MyQuestionsFragment;
import com.doisdoissete.moovup.ui.fragments.questions.QuestionDetailFragment;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by broto on 9/11/15.
 */
@Singleton
public class Navigator {

    @Inject
    public Navigator() {
    }

    public void navigateToLoginActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = new Intent(context, LoginActivity.class);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToGooglePlusActivity(Fragment fragment, int googlePlusResult) {
        if (fragment != null) {
            Intent intentToLaunch = new Intent(fragment.getActivity(), LoginGoogleActivity.class);
            fragment.startActivityForResult(intentToLaunch, googlePlusResult);
        }
    }

    public void navigateToLoginIntroFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(new LoginIntroFragment());
        }
    }

    public void navigateToSignUpFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(new LoginSignUpFragment());
        }
    }

    public void navigateToSignInFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(new LoginSignInFragment());
        }
    }

    public void navigateToMainActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = new Intent(context, MainActivity.class);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToProfileFragment(Context context, String email, String password) {
        if (context != null) {
            ((BaseActivity) context).addFragment(ProfileFragment.newInstance(email, password));
        }
    }

    public void navigateToProfileInputCompanyFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(new SignUpCompanyFragment());
        }
    }

    public void navigateToTermsAndConditionsFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(new TermsFragment());
        }
    }

    public void navigateToGoalCategoriesFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(new GoalCategoriesFragment());
        }
    }

    public void navigateToGoalActivity(Context context, boolean newUser) {
        if (context != null) {
            Intent intentToLaunch = new Intent(context, GoalActivity.class);
            intentToLaunch.putExtra(GoalActivity.ARG_NEW_USER, newUser);

            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToGoalWeightFragment(Context context, int minutes, String periodicityType, Category category) {
        if (context != null) {
            Goal goal = new Goal();
            goal.setTitle(category.getTitle());
            goal.setCategoryId(category.getId());
            goal.setPeriodicityType(periodicityType);
            goal.setTotalMinutes(minutes);

            ((BaseActivity) context).addFragment(GoalWeightFragment.newInstance(goal));
        }
    }

    public void navigateToGoalScheduleFragment(Context context, Category category) {
        if (context != null) {
            ((BaseActivity) context).addFragment(GoalScheduleFragment.newInstance(category));
        }
    }

    public void navigateToFeedFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(FeedFragment.newInstance());
        }
    }

    public void navigateToMyQuestionsFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(MyQuestionsFragment.newInstance());
        }
    }

    public void navigateToContactsFragment(Context context) {

    }

    public void navigateToGoalsFragment(Context context) {

    }

    public void navigateToNotificationsFragment(Context context) {

    }

    public void navigateToHelpFragment(Context context) {

    }

    public void navigateToProfileActivity(Context context) {
        Toast.makeText(context, "Deu Bom Profile", Toast.LENGTH_SHORT).show();
    }

    public void navigateToSettingsFragment(Context context) {
        Toast.makeText(context, "Deu Bom Settings", Toast.LENGTH_SHORT).show();
    }


    public void navigateToGoalAnswerTypeFragment(Context context) {
        if (context != null) {
            ((BaseActivity) context).addFragment(new InterestFragment());
        }
    }

    public void navigateToQuestionDetailsFragment(Context context, Question question) {
        if (context != null) {
            ((BaseActivity) context).addFragment(QuestionDetailFragment.newInstance(question));
        }
    }
}
