package com.doisdoissete.moovup.ui.fragments;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.doisdoissete.moovup.ui.activities.LoginActivity;
import com.doisdoissete.moovup.ui.fragments.login.LoginFormSignUpFragment;

import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;

/**
 * Created by broto on 9/12/15.
 */
@RunWith(AndroidJUnit4.class)
public class SignUpFragmentTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(
            LoginActivity.class);

    @Before
    public void setUp() {
        mActivityRule.getActivity().addFragment(new LoginFormSignUpFragment());
    }
}
