package com.doisdoissete.moovup;

import android.os.StrictMode;

import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import timber.log.Timber;

/**
 * Created by broto on 9/12/15.
 */
public class DebugMoovUpApplication extends MoovUpApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        initStetho();
        initTimber();
        initStrictMode();
    }

    /**
     * Init Leak Canary - Memory Leak tool
     *
     * @return
     */
    protected RefWatcher installLeakCanary() {
//        return LeakCanary.install(this);
        return RefWatcher.DISABLED;
    }

    private void initTimber() {
        Timber.plant(new Timber.DebugTree());
    }

    private void initStetho() {
        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                .build());
    }

    private void initStrictMode() {
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeathOnNetwork()
                .build());
    }
}
